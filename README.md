
RinsLR: Discovery of mid range novel sequence insertions using Long-read Sequencing
======
RinsLR uses both short- and long-read sequences to
characterize mid-range (50 bp to 10 Kbp) novel sequence insertion variants.

# Table of contents
1. [Installation](#installation)
2. [Running RinsLR](#running-rinslr)
3. [Example](#example)
4. [Output Files](#output-files)
5. [Parameter Tuning](#parameter-tuning)

# <a name="installation"></a> Installation

#### Requirements
- gcc and g++: 5.2
- cmake: 3.1
- zlib

Note that RinsLR depends on SPOA, clasp, SMhasher, Parasail, Util-Logger, and Util-Progressbar, which are included in this package. Yet, these tools depend on the following, in addition to RinsLR dependencies:

- cmake: 3.12
- libssl-dev

	## Installation from Source
The first step to install RinsLR is to download the source code from our 
[Bitbucket](https://bitbucket.org/armitasafa/rinslr/) repository. After downloading, 
change the current directory to the source directory ``rinslr`` and run ``make`` in
terminal to create the necessary binary files. 

```shell
git clone https://bitbucket.org/armitasafa/rinslr/ --recursive
cd rinslr
make -j
make install
```																				
# <a name="running-rinslr"></a> Running RinsLR

## Fingerprint Indexing

RinsLR will create a fingerprint index of all long-reads. To run RinsLR indexing, run:

``` rinslr index -l {long-reads} ```

### Options:

| Option | Description |
| ------ | ----------- |
| --long-reads, -l   | path to long-reads fasta file |
| --kmer-size, -k    | length of minimizers (default: 15) |
| --window-size, -w  | size of window (default: 10) - set based on the data set's coverage |
| --output-dir, -d   | path to output directory (default: same as long-reads file) |
| --threads, -t      | number of threads (default: 1) |
| --freq, -f         | percentage of most frequent minimizers to be discarded (default: 0.01) |
| --help, -h         | list of available options and usages |


### Output:

| Output | Description |
| ------ | ----------- |
| \{long-reads\}.fp.idx | Minimizers' locations |
| \{long-reads\}.hv.idx | Minimizers' hash values |
| \{long-reads\}.mt.idx | Metadata |
| \{long-reads\}.sq.idx | Long reads names and ID's |

## Clustering

In this stage, RinsLR creates the cluster file from short-reads, identifying potential breakpoint locations.

```rinslr cluster -s {short-reads}```

### Options:
| Option | Description |
| ------ | ----------- |
| --short-reads-sam, -s | path to short-reads sam file |
| --min-support, -w     | minimum number of reads supporting a breakpoint (default: 10) - set according to dateset coverage |
| --output-dir, -d      | path to output directory (default: current directory) |
| --help, -h            |    list of available options and usages |

### Output:

| Output | Description |
| ------ | ----------- |
| short-reads-cluster | cluster file including potential breakpoint locations and supporting short-reads <br> Each cluster's header contains: <br>\{cluster-id\} \{#-of-short-reads\} \{cluster-range-start\} \{cluster-range-end\} \{chromosome\} |
| short-reads-cluster.count | number of clusters in the cluster file |
| short-reads-cluster.idx | index for random access |

## Long-read Identification

RinsLR uses the cluster file and fingerprint index from the previous stages to identify long-reads covering potential breakpoint locations.

``` rinslr find -l {long-reads} -c {cluster-file} -r {reference-genome} -o {output}```

### Options:

| Option | Description |
| ------ | ----------- |
| --long-reads, -l      | path to long-reads fasta file |
| --output-dir, -d      |  path to output directory (default: current directory ) |
| --cluster-file, -c    |  path to short reads cluster file |
| --reference, -r       |  path to reference genome fasta file |
| --output-prefix, -o   |  output file name prefix |
| --range, -R           |  range of clusters to be processed (default: all) |
| --threads, -t         |    number of threads (default: 1)|
| --index, -i           |  path to index files directory (default: same as long-reads file) |
| --genome-anchor, -x   |  minimum percentage of anchor minimizers present in a candidate long-read (default: 0.3) |
| --insertion-content, -z  | minimum percentage of insertion minimizers present in a candidate long-read (default: 0.05) |
| --chain, -b          | minimum percentage of chain minimizers present in a candidate long-read (default: 0.6) |
| --preset, -p         | technology-specific default values for -x, -z, and -b options (modes: pb (pacbio) and ont (nanopore) |
| --help, -h          | list of available options and usages |

### Outputs:

| Output | Description |
| ------ | ----------- |
| \{output\}   | includes all processed clusters and the sequences from long-reads that cover the potential breakpoints <br> Each cluster's header contains: <br>\{cluster-id\} \{#-of-short-reads\} \{#-of-long-reads\}  \{cluster-range-start\} \{cluster-range-end\} \{chromosome\} \{estimated-insertion-length\} |
| \{output\}.count | number of clusters in the cluster file |
| \{output\}.idx | index for random access |

## Novel Insertion Sequence Detection

In the final stage, RinsLR uses the candidate long-reads from the previous stage to reconstruct the missing sequence and the genomic anchors around it. It then aligns the constructed sequence to the reference for accurate breakpoint detection and novel sequence insertion retrieval.

```rinslr call -l {long-reads} -c {cluster-file} -r {reference-genome} -o {output}```

## Options:

| Option | Description |
| ------ | ----------- |
| --long-reads, -l       | path to long-reads fasta file |
| --output-dir, -d       | path to output directory (default: current directory) |
| --output-prefix, -o    | output file name prefix |
| --reference, -r        | path to reference fasta genome |
| --cluster-file, -c     | path to output from the previous stage|
| --range, -R            | range of clusters to be processed (default: all) |
| --index, -i            | path to index files directory (default: same as long-reads file) |
| --threads, -t          | number of threads (default: 1) |
| --genome-anchor, -x   |  minimum percentage of anchor minimizers present in a candidate long-read (default: 0.3) |
| --insertion-content, -z  | minimum percentage of insertion minimizers present in a candidate long-read (default: 0.05) |
| --chain, -b          | minimum percentage of chain minimizers present in a candidate long-read (default: 0.6) |
| --max-segment, -m      | maximum length of long-read candidate csegments (default: no limit) |
| --preset, -p           | technology-specific default values for -x, -z, and -b options (modes: pb (pacbio) and ont (nanopore) |
| --help, -h             | list of available options and usages |

## Outputs:

| Output | Description |
| ------ | ----------- |
| \{output\}.vcf | vcf file containing all detected insertions |
| \{output\}.log | log file |

## <a name="example"></a>Example
RinsLR can be run on a simulated data set using the commands below:

```
wget https://ndownloader.figshare.com/files/27242969 --output example.tar.gz
tar xzvf example.tar.gz
cd example
rinslr cluster -s sr.sam
rinslr index -l lr.fa
rinslr find -r chr21.fa -l lr.fa -c short-reads-cluster -o example-find -p pb
rinslr call -r chr21.fa -l lr.fa -c example-find -o example -p pb
```

## <a name="parameter-tuning"></a>Parameter Tuning

For parameter tuning scripts, please check the [parameter tuning branch](https://bitbucket.org/armitasafa/rinslr/src/parameter-tuning/) of this repository.