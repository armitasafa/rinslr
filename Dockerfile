FROM ubuntu:20.04
ENV TZ=Europe/Istanbul

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone


RUN apt-get update -y --fix-missing
RUN apt-get upgrade -y

RUN apt-get install git cmake gcc g++ zlib1g-dev libssl-dev -y

RUN mkdir /in
RUN mkdir /out
RUN mkdir /rinslr

# RUN git clone --recursive https://github.com/vpc-ccg/rinslr.git
# git@github.com:vpc-ccg/rinslr.git
COPY rinslr.tar.gz /
RUN tar -zxf /rinslr.tar.gz
RUN cd /rinslr
WORKDIR /rinslr
RUN make && make install && cp rinslr/rinslr /usr/local/bin
RUN cd .. && rm -fr rinslr

RUN apt-get remove git -y
RUN apt-get autoremove -y
VOLUME /in
VOLUME /out
ENTRYPOINT ["/usr/local/bin/rinslr"]
