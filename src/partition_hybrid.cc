#include <iostream>
#include <string>
#include <vector>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <fstream>
#include <algorithm>
#include <climits>
#include "common.h"
#include "progressbar.h"
#include "partition_hybrid.h"

#define MAXB  809600

using namespace std;

genome_partition_hybrid::genome_partition_hybrid(const string & out_prefix, bool write_to_files, int support) {
    MIN_SUPPORT = support;
    if (write_to_files) {
        partition_out_file = fopen((out_prefix + "/short-reads-cluster").c_str(), "wb");
        if (partition_out_file == NULL){
            Logger::instance().info("[Genome Partition] Cannot open the file.\n");
            exit(1);
        }
        partition_out_index_file = fopen((out_prefix + "/short-reads-cluster.idx").c_str(), "wb");
        if (partition_out_index_file == NULL){
            Logger::instance().info("[Genome Partition] Cannot open the file.\n");
            exit(1);
        }
        partition_out_count_file = fopen((out_prefix + "/short-reads-cluster.count").c_str(), "w");
        if (partition_out_count_file == NULL){
            Logger::instance().info("[Genome Partition] Cannot open the file.\n");
            exit(1);
        }
    }
}

genome_partition_hybrid::genome_partition_hybrid(const string &partition_file_path, const string &range, bool write_to_file):
                                                    genome_partition_hybrid(range, write_to_file) {
    // extracting range [start,end]
    size_t pos;
    start = stoi(range, &pos);
    if (pos < range.size()) {
        end = stoi(range.substr(pos + 1));
    } else {
        end = start;
    }

    // reading the index file of partitions
    FILE *partition_file_index = fopen((partition_file_path + ".idx").c_str(), "rb");
    if (partition_file_index == NULL) {
        Logger::instance().info("[Genome Partition] Cannot open the file.\n");
        exit(1);
    }

    Logger::instance().info("Loading the index file.\n");
    vector<size_t> offsets;
    size_t offset;
    while (fread(&offset, 1, sizeof(size_t), partition_file_index) == sizeof(size_t)) {
        offsets.push_back(offset);
    }

    fclose(partition_file_index);

    if (start < 1)
        start = 1;
    if (end > offsets.size())
        end = offsets.size();

    total = end - start + 1;

    partition_file = fopen(partition_file_path.c_str(), "rb");
    fseek(partition_file, offsets[start - 1], SEEK_SET);
}

void genome_partition_hybrid::dump_cluster(vector<pair<string, string> > reads, int range_start, int range_end) {
	if (reads.size() == 0)
		return;
    size_t pos = ftell(partition_out_file);
    fwrite(&pos, 1, sizeof(size_t), partition_out_index_file);
    fprintf(partition_out_file, "%d %d %d %d %s\n", partition_id, reads.size(), range_start,
            range_end, curr_chr.c_str());
    for (int i = 0; i < reads.size(); i++) {
        fprintf(partition_out_file, "%s %s 1 1\n", reads[i].first.c_str(), reads[i].second.c_str());
    }
    partition_count++;
    partition_id++;
}

int genome_partition_hybrid::clean_up(vector<BreakpointCandidate>& locs, int offset, bool process_full, int next_offset) {
    vector<pair<string, string> > curr_reads;

    int sum = 0;
    for (int i = 0; i < 11; i++) {
        sum += locs[i].support;
    }
    locs[MARGIN].window_support = sum;
   if (locs[MARGIN].window_support >= MIN_SUPPORT)
       locs[MARGIN].nbr_stretch = 0;

    for (int i = MARGIN + 1; i < WINDOW_SIZE; i++) {
        locs[i].window_support = locs[i - 1].window_support - locs[i - MARGIN - 1].support + locs[i + MARGIN].support;
        if (locs[i].window_support >= MIN_SUPPORT)
            locs[i].nbr_stretch = 0;
        else
            locs[i].nbr_stretch = locs[i - 1].nbr_stretch + 1;
    }

    int last_point = WINDOW_SIZE;
    if (!process_full) {
        int start = WINDOW_SIZE;
        if (next_offset - offset < WINDOW_SIZE) {
            start = next_offset - offset;
		}
		last_point = start - 1;
        for (int i = start - MARGIN; i > 0; i--) {
            if (locs[i].nbr_stretch > MIN_SUPPORT) {
                last_point = i - MIN_SUPPORT;
                break;
            }
        }
    }

    int stretch_start = -1, stretch_end = -1;
    vector<pair<string, string> > reads;
    for (int i = 0; i < last_point; i++) {
        if (locs[i].nbr_stretch == 0) {
            if (stretch_start == -1) {
                stretch_start = i;
                reads.insert(reads.end(), locs[i].reads.begin(), locs[i].reads.end());
            }
            else {
                reads.insert(reads.end(), locs[i].reads.begin(), locs[i].reads.end());
            }
        }
        else {
            if (stretch_start != -1) {
                stretch_end = i - 1;
                int breakpoint_location = offset + (stretch_end + stretch_start)/2;
                dump_cluster(reads, max(breakpoint_location - CLUSTER_SIZE, 0),
                             breakpoint_location + CLUSTER_SIZE);
                try_to_merge(max(breakpoint_location - CLUSTER_SIZE, 0), breakpoint_location + CLUSTER_SIZE, reads);
                stretch_start = -1;
                stretch_end = -1;
                reads.clear();
            }
        }
    }

    int j = 0;
    if (!process_full) {
        for (int i = last_point; i < WINDOW_SIZE; i++) {
            locs[j] = locs[i];
			if (j < MIN_SUPPORT)
            	locs[j].nbr_stretch = 1;
            j++;
        }
    }
    for (int i = j; i < WINDOW_SIZE; i++) {
        BreakpointCandidate tmp;
        locs[i] = tmp;
    }

    return j;
}

void genome_partition_hybrid::try_to_merge(int cluster_start, int cluster_end, vector<pair<string, string> >
        cluster_reads) {
    if (potential_cluster.first == -1) {
        potential_cluster.first = cluster_start;
        potential_cluster.second = cluster_end;
    }
    if ((cluster_start < potential_cluster.second) && (abs(potential_cluster.second - cluster_end) <
        MERGE_RANGE)) {
        potential_cluster.second = cluster_end;
        potential_reads.insert(potential_reads.end(), cluster_reads.begin(), cluster_reads.end());
        found_merged_cluster = true;
    }
    else {
        if (found_merged_cluster) {
            dump_cluster(potential_reads, potential_cluster.first, potential_cluster.second);
            potential_reads.clear();
        }
        potential_cluster.first = cluster_start;
        potential_cluster.second = cluster_end;
        found_merged_cluster = false;
    }
}

pair<string, int> inline genome_partition_hybrid::fill(vector<BreakpointCandidate>& locs, int genome_offset,
                                         ifstream& fin) {
    do {
        sam_record record(line);

    if ((!record.is_fully_mapped) && ((record.flag & 2048) != 2048) && (record.mapq > MIN_Q)) {
            if (record.rname != curr_chr) {
                return {record.rname, record.pos};
            }

            if (record.pos >= genome_offset + WINDOW_SIZE || record.end_pos >= genome_offset + WINDOW_SIZE) {
                return {record.rname, record.pos};
            }

            if (record.head_clip_range != 0) {
                locs[record.pos - genome_offset].support++;
                locs[record.pos - genome_offset].reads.push_back({record.qname, record.seq});
                locs[record.pos - genome_offset].chr = record.rname;
            }

            if (record.tail_clip_range != 0) {
                locs[record.end_pos - genome_offset - 1].support++;
                locs[record.end_pos - genome_offset - 1].reads.push_back({record.qname, record.seq});
                locs[record.end_pos - genome_offset - 1].chr = record.rname;
            }

            if (record.insertion_pos != record.pos) {
                locs[record.insertion_pos - genome_offset].support++;
                locs[record.insertion_pos - genome_offset].reads.push_back({record.qname, record.seq});
                locs[record.insertion_pos - genome_offset].chr = record.rname;
            }
        }
    }
    while (getline(fin, line));

    return {"end", INT_MAX};
}

void genome_partition_hybrid::cluster_reads(string map_path) {
	map<string, int> chr_len;

	ProgressBar progress(80);

    ifstream fin;
    fin.open(map_path);

    vector<BreakpointCandidate> locs;
    for (int i = 0; i < WINDOW_SIZE + MARGIN; i++) {
        BreakpointCandidate tmp;
        locs.push_back(tmp);
    }

	bool show_progress = true;
    string next_chr;
    int next_genome_offset;
    bool process_full = false;

    string line;
	int length;
	uint64_t sum = 0;
    getline(fin, line);

    //Reading chromosome lengths from sam header
    while (line[0] == '@') {
		if (line.substr(0, 3) == "@SQ") {
			int s = line.find("SN");
			int e = line.find("LN");
			string name = "";
			for (int i = s + 3; i < line.size(); i++) {
				if (line[i] == ' ' || line[i] == '\t')
					break;
				name += line[i];
			}
			string ln = "";
			for (int i = e+3; i < line.size(); i++) {
				if (line[i] == ' ' || line[i] == '\t')
					break;
				ln += line[i];
			}
			length = stoi(ln);
			sum += length;
			chr_len.insert({name, length});
		}
		getline(fin, line);
	}
    if (chr_len.size() == 0) {
		show_progress = false;
		Logger::instance().info("No sam header detected, progress will not be shown. Please wait until partitioning finishes.\n");
	}

	string comment = "Partitioning";
	if (show_progress)
		progress.update((0.0/(float)sum) * 100, comment);

	int genome_offset = GENOME_OFFSET_INIT;
	int array_offset = ARRAY_OFFSET_INIT;

    sam_record record(line);
    curr_chr = record.rname;
    genome_offset = record.pos - array_offset;

	uint64_t processed = 0;
    while (!fin.eof()) {
		if (show_progress)
			progress.update(((float)(processed + genome_offset)/(float)sum) * 100, comment);
        process_full = false;
        auto next = fill(locs, genome_offset, fin);
        if (next.first != curr_chr || (next.first == curr_chr && genome_offset + WINDOW_SIZE < next.second)) {
            next_genome_offset = next.second - 5;
            process_full = true;
            next_chr = next.first;
        }
        array_offset = clean_up(locs, genome_offset, process_full, next.second);
        if (process_full) {
            genome_offset = next_genome_offset;
            if (curr_chr != next_chr) {
				processed += chr_len[curr_chr];
				if (found_merged_cluster)
				    dump_cluster(potential_reads, potential_cluster.first, potential_cluster.second);
            }
            curr_chr = next_chr;
            potential_cluster = {-1, -1};
            found_merged_cluster = false;
        }
        else
            genome_offset += WINDOW_SIZE - array_offset;
    }

	if (show_progress)
		progress.update(100.0, comment);
}

genome_partition_hybrid::~genome_partition_hybrid () {
    if (partition_file != NULL) {
        fclose(partition_file);
    }
    if (partition_out_file != NULL) {
        fclose (partition_out_file);
    }
    if (partition_out_index_file != NULL) {
        fclose (partition_out_index_file);
    }
    if (partition_out_count_file != NULL) {
        fprintf(partition_out_count_file, "%d\n", partition_count);
        fclose (partition_out_count_file);
    }
}


int genome_partition_hybrid::get_start() {
	return p_start;
}

int genome_partition_hybrid::get_end() {
	return p_end;
}

int genome_partition_hybrid::get_id() {
	return partition_id;
}

//read next partition
vector <pair<pair < string, string>, pair<int, int>>> genome_partition_hybrid::read_partition() {

	int sz, i;
	char pref[MAXB];
	char name[MAXB], read[MAXB];

	if (start > end)
		return vector < pair < pair < string, string >, pair < int, int >> > ();
	partition_count++;
	start++;
	fscanf(partition_file, "%d %d %d %d %s\n", &partition_id, &sz, &p_start, &p_end, pref);
	p_ref = pref;
	current_cluster.resize(0);
	current_cluster.reserve(sz);

	for (i = 0; i < sz; i++) {
		fgets(pref, MAXB, partition_file);
		int loc, support;
		sscanf(pref, "%s %s %d %d", name, read, &support, &loc);
		if (loc != -1)
		    current_cluster.push_back({{string(name), string(read)},
			    					   {support, loc}});
	}
	return current_cluster;
}