#include "cut_ranges.h"

#include <cstring>
#include <fstream>
#include <sstream>
#include <iostream>
#include <iterator>
#include <algorithm>

#include "logger.h"

using namespace std;

//TODO merge finding cuts

cut_ranges::cut_ranges() {}

cut_ranges::cut_ranges(const string &lrPath, const string &ranges_path, bool build_index) : lr_path(lrPath) {
    string seq_file = lrPath + ".sq.idx";
    Logger::instance().info("Loading sequence names from: %s\n", seq_file.c_str());
    ifstream seqin(seq_file, ios::in | ios::binary);
    if (!seqin) {
        Logger::instance().info("Could not load file. Quitting.\n");
        exit(1);
    }

    string name;
    offset_t len;
    uint8_t name_size;
    while(seqin.read(reinterpret_cast<char*>(&name_size), sizeof(uint8_t))) {
        name.resize(name_size);
        seqin.read((char*)(name.c_str()), name_size);
        seqin.read(reinterpret_cast<char*>(&len), sizeof(uint16_t));
        names.push_back(name);
    }
    seqin.close();

    Logger::instance().info("Loading ranges from: %s\n", ranges_path.c_str());
    ifstream ranges_file(ranges_path, ios::in | ios::binary);
    if (!ranges_file) {
        Logger::instance().info("Could not load file. Quitting.\n");
        exit(1);
    }

    uint32_t size;
    ranges_file.read(reinterpret_cast<char*>(&size), sizeof(uint32_t));
    ranges.resize(size);
    ranges_file.read(reinterpret_cast<char*>(&ranges[0]), size * sizeof(pair<read_id_t, range_s>));
    ranges_file.close();
}

cut_ranges::cut_ranges(const string &lrPath, const string &seq_file, vector<pair<read_id_t, range_s> >& range_offsets) : lr_path(lrPath) {
    Logger::instance().info("Loading sequence names from: %s\n", seq_file.c_str());
    ifstream seqin(seq_file, ios::in | ios::binary);
    if (!seqin) {
        Logger::instance().info("Could not load file. Quitting.\n");
        exit(1);
    }

    string name;
    offset_t len;
    uint8_t name_size;
    int i = 0;
    while(seqin.read(reinterpret_cast<char*>(&name_size), sizeof(uint8_t))) {
        name.resize(name_size);
        seqin.read((char*)(name.c_str()), name_size);
        seqin.read(reinterpret_cast<char*>(&len), sizeof(offset_t));
        names.push_back(name);
        i++;
    }
    seqin.close();

    ranges = range_offsets;
}

cut_ranges::cut_ranges(const string &lrPath, bool build_index) : lr_path(lrPath) {
    if (build_index) {
        string lr_name;
        lr_file.open(lr_path);
        string line;

        while (!lr_file.eof()) {
            getline(lr_file, line);
            if (line[0] == '>') {
                lr_name = line.substr(1);
                read_offsets.insert({lr_name, lr_file.tellg()});
                getline(lr_file, line);
            }
        }
    }
    lr_file.close();
    lr_file.open(lr_path);
}

void cut_ranges::extract() {
    if (ranges.size() == 0)
        return;

    ifstream fin;
    fin.open(lr_path);
    string line;

    int curr_read = 0;
    vector<pair<read_id_t, range_s> >::iterator search_read = ranges.begin();

    while ((*search_read).first == -1) {
        search_read++;
    }

	// FILE* ef = fopen("ef.txt", "w");
    while (!fin.eof() && search_read != ranges.end()) {
        getline(fin, line);
        getline(fin, line);
        if (curr_read == (*search_read).first) {
			// fprintf(ef, "%d\t%d\n", curr_read, line.size());
            string read = line.substr((*search_read).second.start, (*search_read).second.end);
            reads.push_back({curr_read, {(*search_read).second.start, read}});
            search_read++;
        }
        curr_read++;
    }
	// fclose(ef);
}

read_cut cut_ranges::find_read(read_id_t id) {
    read_cut dummy;
    dummy.first = id;
    vector<read_cut>::iterator it = std::lower_bound(reads.begin(), reads.end(), dummy);
    return (*it);
}

pair<string, string> cut_ranges::get_cut(read_id_t lr_id, offset_t start, offset_t end) {
    string cut;
    read_cut read = find_read(lr_id);
	// cout << names[lr_id] << " | " << start << " - " << end << " | " << read.second.first << endl;
	// if (start < read.second.first || end - start > start - read.second.first, end - start) {
	// 	return {names[lr_id], ""};
	// }
    cut = read.second.second.substr(start - read.second.first, end - start);
	return {names[lr_id], cut};
}

string cut_ranges::get_cut(string name, offset_t start, offset_t end) {
    string cut;
    lr_file.seekg(read_offsets[name]);
    string line;
    getline(lr_file, line);
    Logger::instance().debug(">%s\n%s\n", name.c_str(), line.c_str());
    cut = line.substr(start, end - start - 1);
    return cut;
}

int cut_consensus(vector<string>& msa, int begin, int step_size, int step_num, int threshold) {
    int i = begin;
    int step = 0;
    while (step < step_num) {
        int gap_cnt = 0;
        for (int j = 0; j < msa.size(); j++) {
            if (msa[j][i] == '-')
                gap_cnt++;
        }
        if (gap_cnt < threshold) {
            return i;
        }
        i += step_size;
        step++;
    }
    return begin + step_num * step_size;
}

std::pair<std::string, std::pair<int, int>> cut_consensus_bimodal(vector<string> alignments, int left_reads,
                                                                  int right_reads, int bimodal_reads) {
    int left, right;
    int step_size = 5;
    int th_left = alignments.size() - 1 - 0.5 * (bimodal_reads + left_reads);
    int th_right = alignments.size() - 1 - 0.5 * (bimodal_reads + right_reads);
    string consensus = alignments[alignments.size() - 1];

    left = cut_consensus(alignments, 0, step_size, min(200, (int)consensus.size()/2)/step_size, th_left);
    right = cut_consensus(alignments, consensus.size() - 1, -step_size, min(200, (int)consensus.size()/2)/step_size, th_right);

    pair<int, int> ans = {left, right};
    string cut = consensus.substr(left, right - left);
    cut.erase(std::remove(cut.begin(), cut.end(), '-'), cut.end());
    return {cut, ans};
}

std::pair<std::string, std::pair<int, int>> cut_consensus_single(vector<string> alignments, float threshold) {
    int left, right;
    int step_size = 5;
    int th = threshold * alignments.size();
    string consensus = alignments[alignments.size() - 1];

    left = cut_consensus(alignments, 0, step_size, min(200, (int)consensus.size()/2)/5, th);
    right = cut_consensus(alignments, consensus.size() - 1, -step_size, min(200, (int)consensus.size()/2)/5, th);

    pair<int, int> ans = {left, right};
    string cut = consensus.substr(left, right - left);
    cut.erase(std::remove(cut.begin(), cut.end(), '-'), cut.end());
    return {cut, ans};
}

