#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <cstdio>
#include <cassert>
#include <zlib.h>
#include <cstring>
#include <cstdlib>
#include "common.h"
#include "p2_partition.h"
#define MAXB  809600

using namespace std;

//OPEN WRITE FILE
p2_partition::p2_partition (const string & out_prefix, bool write_to_files) {
    if(write_to_files){
        partition_out_file = fopen(out_prefix.c_str(), "wb");
        if (partition_out_file == NULL){
            Logger::instance().info("[Genome Partition-p2] Cannot open the file.\n");
            exit(1);
        }
        partition_out_count_file = fopen((out_prefix + ".count").c_str(), "w");
        if (partition_out_count_file == NULL){
            Logger::instance().info("[Genome Partition-p2] Cannot open the file.\n");
            exit(1);
        }
    }
}

//READ
p2_partition::p2_partition (const string &partition_file_path, const string &range, map<int, size_t>& offsets) {
    size_t pos;
    start = stoi (range, &pos);
//    if (pos < range.size()) {
        end = stoi (range.substr(pos+1));
//    } else {
//        end = start;
//    }

    total = end - start + 1;

//    if (end > total)
//        end = total;

    cluster_offsets = offsets;

    partition_file = fopen(partition_file_path.c_str(), "rb");
    fseek(partition_file, cluster_offsets[start-1], SEEK_SET);
}

//CALL ON EACH CLUSTER
void p2_partition::add_cuts(vector<pair<pair<string, string>, pair<int,int> > >& short_reads,
                            vector<cut>& cuts,
                            int p_start, int p_end, string p_ref, int insertion, int id) {
    partition_count++;
    size_t pos = ftell(partition_out_file);
    cluster_offsets.insert({id, pos});
    fprintf(partition_out_file, "%d %lu %lu %d %d %s %d\n", id, short_reads.size(), cuts.size(), p_start, p_end, p_ref
    .c_str(), insertion);
    for (auto &i: short_reads) {
        fprintf(partition_out_file, "%s %s %d %d\n", i.first.first.c_str(), i.first.second.c_str(), i.second.first,
                i.second.second);
    }
    for (auto i = cuts.begin(); i != cuts.end(); i++) {
        fprintf(partition_out_file, "%d %d %d %d %d\n", i->seq_id, i->range.start, i->range.end, i->type, i->orientation);
    }
    partition_id++;
}

p2_partition::~p2_partition (){
    if (!closed) {
        if (partition_file != NULL){
            fclose(partition_file);
        }
        if (partition_out_file != NULL){
            fclose (partition_out_file);
        }
        if (partition_out_count_file != NULL){
            fprintf(partition_out_count_file, "%d\n", partition_count);
            fclose (partition_out_count_file);
        }
    }
}

void p2_partition:: close(){
    if (partition_file != NULL){
        fclose(partition_file);
    }
    if (partition_out_file != NULL){
        fclose (partition_out_file);
    }
    if (partition_out_count_file != NULL){
        fprintf(partition_out_count_file, "%d\n", partition_count);
        fclose (partition_out_count_file);
    }
    closed = true;
}

int p2_partition::get_range_start ()
{
    return start;
}

int p2_partition::get_start ()
{
    return p_start;
}

int p2_partition::get_end ()
{
    return p_end;
}

string p2_partition::get_reference ()
{
    return p_ref;
}

int p2_partition::get_id () {
    return partition_id;
}

int p2_partition::get_old_id () {
    return 0;
}

int p2_partition::get_total() {
    return total;
}

int p2_partition::get_estimated_insertion() {
    return estimated_insertion;
}

map<int, size_t> p2_partition::get_cluster_offsets() {
    return cluster_offsets;
}

//read next partition
pair<vector<pair<pair<string, string>, pair<int,int> > >, vector<p2_read_s> >
        p2_partition::read_partition() {

    int sr_sz, cut_sz, i;
    char pref[MAXB];
    char name[MAXB], read[MAXB];
    if (start > end)
        return pair<vector<pair<pair<string, string>, pair<int,int> > >, vector<p2_read_s> > ();

    fseek(partition_file, cluster_offsets[start], SEEK_SET);
    fscanf(partition_file, "%d %d %d %d %d %s %d\n", &partition_id, &sr_sz, &cut_sz, &p_start, &p_end, pref, &estimated_insertion);

    partition_count++;
    start++;
    p_ref = pref;
    cut_candidates.resize(0);
    cut_candidates.reserve(cut_sz);
    short_reads.resize(0);
    short_reads.reserve(sr_sz);

    for (i = 0; i < sr_sz; i++) {
        fgets(pref, MAXB, partition_file);
        int loc, support;
        sscanf(pref, "%s %s %d %d", name, read, &support, &loc);
        short_reads.push_back({{string(name), string(read)}, {support, loc}});
    }

    for (i = 0; i < cut_sz; i++) {
        fgets(pref, MAXB, partition_file);
        int id, start_pos, end_pos, type, orientation;
        sscanf(pref, "%d %d %d %u %u", &id, &start_pos, &end_pos, &type, &orientation);
        range_s range = {static_cast<offset_t>(start_pos), static_cast<offset_t>(end_pos)};
        cut_candidates.push_back((p2_read_s) {.id = static_cast<read_id_t>(id), .range = range, .type = static_cast<type_en>(type),
                                              .orientation = static_cast<orientation_en>(orientation)});
    }

    return {short_reads, cut_candidates};
}