#include "insertion_assembler.h"

#include <mutex>
#include <string>
#include <iterator>
#include <algorithm>
#include "common.h"

using namespace std;

mutex extract_lock;

InsertionAssembler::InsertionAssembler(Sketch* sketch, cut_ranges* read_extractor) {
    lr_sketch = sketch;
    extractor = read_extractor;
}

string InsertionAssembler::build_segment(vector<string> &cuts) {
    spoa::Graph graph{};
    graph.Clear();
//    auto alignment_engine = spoa::AlignmentEngine::Create(spoa::AlignmentType::kSW, 2, -32, -64, -1);
    auto alignment_engine = spoa::AlignmentEngine::Create(spoa::AlignmentType::kSW, 10, -2, -15, -7);
    for (int i = 0; i < cuts.size(); i++) {
        auto alignment = alignment_engine->Align(cuts[i], graph);
        graph.AddAlignment(alignment, cuts[i]);
    }
    vector<string> msa = graph.GenerateMultipleSequenceAlignment(true);

    pair<string, pair<int, int>> cut_range = cut_consensus_single(msa, 0.5);
    int left = cut_range.second.first;
    int right = cut_range.second.second;

    string consensus = msa[msa.size() - 1];

#ifdef DEBUG
    Logger::instance().debug("- MSA\n");
    for (int i = 0; i < msa.size(); i++) {
        string tmp = msa[i].insert(left, "|");
        string tmp2 = tmp.insert(right + 1, "|");
        Logger::instance().debug("%s\n", tmp2.c_str());
    }
    Logger::instance().debug("\n");
#endif

    string cut = consensus.substr(left, right - left);
    cut.erase(std::remove(cut.begin(), cut.end(), '-'), cut.end());

    return cut;
}

vector<string> InsertionAssembler::extract_reads(vector<cut>& cuts) {
    vector<string> ext;
    for (auto i = 0; i < cuts.size(); i++) {
        extract_lock.lock();
        string r = extractor->get_cut(lr_sketch->sequences[cuts[i].seq_id].first, cuts[i].range.start, cuts[i].range.end);
        extract_lock.unlock();

        if (cuts[i].orientation == REV)
            ext.push_back(reverse_complement(r));
        else
            ext.push_back(r);
    }
    sort(ext.begin(), ext.end(), [](const auto &a, const auto &b) { return a.size() > b.size();});
    return ext;
}

void inline get_range(int& start, int& end, int s1, int e1, int s2, int e2, int l) {
    start = max(min(s1, e2) - 100, 0);
    end = min(max(s1, e2), l);
}

map<read_id_t, cut> InsertionAssembler::check_end(map<read_id_t, cut> &l, map<read_id_t, cut> &r) {
    map<read_id_t, cut> mid;
    for (auto it = l.begin(); it != l.end(); it++) {
        auto f = r.find(it->first);
        if (f != r.end() && it->second.orientation == f->second.orientation) {
            offset_t start, end;
            read_id_t id = it->first;
            if (it->second.orientation == FRW) {
                start = max(min(it->second.range.start, f->second.range.end) - 100, 0);
                end = min(max(it->second.range.start, f->second.range.end) + 100, (int)lr_sketch->sequences[it->first].second);
            }
            else {
                start = max(min(it->second.range.end, f->second.range.start) - 100, 0);
                end = min(max(it->second.range.end, f->second.range.start) + 100, (int)lr_sketch->sequences[it->first].second);
            }
            range_s range = {start, end};
            cut merged_cut;
            merged_cut.range = range;
            merged_cut.seq_id = id;
            merged_cut.orientation = it->second.orientation;
            mid.insert({id, merged_cut});
            Logger::instance().debug("%s: %d - %d\n", lr_sketch->sequences[it->first].first.c_str(), start, end);
        }
    }
    return mid;
}

string InsertionAssembler::get_overlap(vector<string>& l, vector<string>& r, string& m) {
    spoa::Graph graph{};
    auto alignment_engine = spoa::AlignmentEngine::Create(spoa::AlignmentType::kOV, 2, -32, -64, -1);
    graph.Clear();
    for (int i = 0; i < l.size(); i++) {
        auto alignment = alignment_engine->Align(l[i], graph);
        graph.AddAlignment(alignment, l[i]);
    }
    if (m != "") {
        auto alignment = alignment_engine->Align(m, graph);
        graph.AddAlignment(alignment, m);
    }
    for (int i = r.size() - 1; i >= 0; i--) {
        auto alignment = alignment_engine->Align(r[i], graph);
        graph.AddAlignment(alignment, r[i]);
    }

    auto msa = graph.GenerateMultipleSequenceAlignment(true);

    #ifdef DEBUG
    Logger::instance().debug("- MSA\n");
    for (int i = 0; i < msa.size(); i++) {
        Logger::instance().debug("%s\n", msa[i].c_str());
    }
    Logger::instance().debug("\n");
    #endif

    string consensus = msa[msa.size() - 1];
    consensus.erase(std::remove(consensus.begin(), consensus.end(), '-'), consensus.end());

    return consensus;
}

pair<string, int> InsertionAssembler::assemble(vector<string>& left_reads, vector<string>& right_reads, string &output) {
    output += "--- Building Long Insertion ---";

    string left_seg, right_seg, lanchor, ranchor, lcheck, rcheck;
    vector<string> lsegs, rsegs;
    unordered_set<hash_t> l_minimizers_frw, l_minimizers_rev, r_minimizers_frw, r_minimizers_rev;
    int cut_size, support = left_reads.size() + right_reads.size(), anchor_cut;
    int step = 1;

    string l_tmp, r_tmp;

    vector<cut> read_cuts;
    vector<cut> lcuts, rcuts, bimodal_cuts;

    bool overlapping_f = false;

    while (true) {
        Logger::instance().debug("STEP %d\n", step);
        if (step == 10) {
            output += "\n===> Could not build insertion properly in " + to_string(step) + " steps.";
            break;
        }

#ifdef DEBUG
        Logger::instance().debug("----------------\n");
        Logger::instance().debug("STEP %d\n", step);
#endif
        support += left_reads.size() + right_reads.size();

        left_seg = build_segment(left_reads);
        if (left_seg == "")
            break;
        lsegs.push_back(left_seg);
        anchor_cut = min(600, (int)left_seg.size());
        lanchor = left_seg.substr(left_seg.size() - anchor_cut, anchor_cut - 1);
        right_seg = build_segment(right_reads);
        if (right_seg == "")
            break;
        rsegs.push_back(right_seg);
        anchor_cut = min(600, (int)right_seg.size());
        ranchor = right_seg.substr(0, anchor_cut - 1);

        unordered_set<hash_t> dummy;
        vector<string> reads;
        pair<vector<cut>, int> cut_results;
        cut_results = lr_sketch->find_cuts_all(lanchor, ranchor, true, reads);
        read_cuts = cut_results.first;

        for (int i = 0; i < read_cuts.size(); i++) {
            if (read_cuts[i].type == PARTIAL_LEFT && lcuts.size() < 7)
                lcuts.push_back(read_cuts[i]);
            else if (read_cuts[i].type == PARTIAL_RIGHT && rcuts.size() < 7)
                rcuts.push_back(read_cuts[i]);
            else if (read_cuts[i].type == BIMODAL && bimodal_cuts.size() < 7)
                bimodal_cuts.push_back(read_cuts[i]);
        }

        if (bimodal_cuts.size() > 2) {
            output += "\n===> Insertion built in "+ to_string(step) + " steps.";
            break;
        }
        else if (cut_results.second == -1) {
            Logger::instance().debug("Overlap\n");
            overlapping_f = true;
            for (int i = 0; i < read_cuts.size(); i++) {
                if (read_cuts[i].type == OVERLAPPING_READ) {
                    bimodal_cuts.push_back(read_cuts[i]);
                }
            }
            break;
        }

        left_reads = extract_reads(lcuts);
        Logger::instance().debug("-----\n");
        right_reads = extract_reads(rcuts);

        lcuts.clear();
        rcuts.clear();
        bimodal_cuts.clear();

        output += "\n* " + to_string(step) + ":\n";
        output += "          Left Segment :  " + left_seg + "\n";
        output += "          Right Segment:  " + right_seg + "\n";
        output += "          Picked " + to_string(left_reads.size()) + " new left cuts\n";
        output += "          Picked " + to_string(right_reads.size()) + " new right cuts\n";

        step += 1;
    }

    vector<string> middle_reads;
    string middle = "";
    if (!overlapping_f) {
        middle_reads = extract_reads(bimodal_cuts);
        middle = build_segment(middle_reads);
    }
    else {
        middle_reads = extract_reads(bimodal_cuts);
        middle = build_segment(middle_reads);
    }
    support += middle_reads.size();

#ifdef DEBUG
    Logger::instance().debug("SEGMENTS:\n");
    for (int i = 0; i < lsegs.size(); i++)
        Logger::instance().debug(">l%d\n%s\n", i+1, lsegs[i].c_str());
    for (int i = 0; i < rsegs.size(); i++)
        Logger::instance().debug(">r%d\n%s\n", i+1, rsegs[i].c_str());
    Logger::instance().debug(">m\n%s\n", middle.c_str());
#endif

    string insertion = get_overlap(lsegs, rsegs, middle);

    return {insertion, support};
}