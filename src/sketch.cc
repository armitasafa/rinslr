#include <cmath>
#include <climits>
#include <deque>
#include <mutex>
#include <thread>
#include <string>
#include <iostream>
#include <algorithm>
#include <filesystem>
#include "logger.h"
#include "common.h"
#include "MurmurHash3.h"

#include "sketch.h"

#include <chrono>

using namespace std;
using namespace std::chrono;

ProgressBar sketch_progress(80);
ProgressBar stats_progress(80);
ProgressBar freq_progress(80);
ProgressBar sort_progress(80);
uint64_t size_read = 0;
int sort_completed = 0;
int freq_completed = 0;

int tmp_file_no = 0;
int PASS = 0;

const int MERGE_BUFF = 10000000;

mutex read_mutex;

Sketch::Sketch() {}

ifstream::pos_type filesize(const string filename) {
    ifstream in(filename, ifstream::ate | ifstream::binary);
    return in.tellg();
}

Sketch::Sketch(string lp, string dp, int t, int k, int w, float df) {
    kmer_size = k;
    window_size = w;
    DISCARD_FREQ = df;
    lr_path = lp;
    dp_path = dp;
    thread_cnt = t;
    file_name = get_file_name(lp);

    gz_fin = gzopen(lr_path.c_str(), "rb");
    if (!gz_fin) {
        Logger::instance().info("Error: Could not open file. Quitting.\n");
        exit(1);
    }

    file_size = filesize(lr_path);
    zbuffer = (char *) malloc(BUFFSIZE);

    tmp_file_name = dp_path + "/temp" + to_string(rand()) + "-";

    build_sketch();
}

Sketch::Sketch(string lp, string dp) {
    lr_path = lp;
    dp_path = dp;
    file_name = get_file_name(lp);
    load();
    claspChain = ClaspChain();
}

void Sketch::dump(unordered_set<hash_t> &discard_set) {
    string input = tmp_file_name + to_string(tmp_file_no) + "-0" + ".fp.idx";
    ifstream tfin(input, ios::ate | ios::binary);
    uint64_t size = tfin.tellg()/sizeof(pair<hash_t, Location>);
    ifstream fin(input, ios::in | ios::binary);

    vector<pair<hash_t, Location> > ref_minimizers_vec;
    ref_minimizers_vec.resize(size);
    fin.read(reinterpret_cast<char*>(&ref_minimizers_vec[0]), size * sizeof(pair<hash_t, Location>));
    fin.close();
    remove(input.c_str());

    ofstream fout(dp_path + "/" + file_name + ".fp.idx", ios::out | ios::binary);

    if (!fout) {
        Logger::instance().info("Could not create file. Quitting.\n");
        exit(1);
    }

    ProgressBar write_prog(80);
    write_prog.update(0.0, "Writing Sketch ");

    uint64_t hash_entries = 0;
    hash_t prv_hash;
    vector<minimizer> hash_sizes;

    int write_buf_size = 0;
    Location* write_buf = (Location*)malloc(1000000 * sizeof(Location));

    uint64_t cnt = 0;

    fout.write((char*)&kmer_size, sizeof(uint64_t));
    fout.write((char*)&window_size, sizeof(uint64_t));
    fout.write((char*)&total_entries, sizeof(uint64_t));

    if (!ref_minimizers_vec.empty()) {
        uint64_t i = 0;
        while (discard_set.find(ref_minimizers_vec[i].first) != discard_set.end()) {
            i++;
        }
        write_buf[write_buf_size++] = ref_minimizers_vec[i].second;
        prv_hash = ref_minimizers_vec[i].first;
        hash_entries = 1;
		i++;

        for (; i < ref_minimizers_vec.size(); i++) {
            if (discard_set.find(ref_minimizers_vec[i].first) != discard_set.end()) {
                continue;
            }

            if (ref_minimizers_vec[i].first == prv_hash)
                hash_entries++;
            else {
                hash_sizes.push_back((minimizer){.hash_value = prv_hash, .offset = cnt});
                cnt += hash_entries;
                prv_hash = ref_minimizers_vec[i].first;
                hash_entries = 1;
            }
            if (write_buf_size == 1000000) {
                fout.write((char*)&write_buf[0], 1000000 * sizeof(Location));
                write_buf_size = 0;
                write_prog.update(((float)cnt/(float)total_entries) * 100, "Writing Sketch ");
            }
            write_buf[write_buf_size++] = ref_minimizers_vec[i].second;
        }

		if (hash_sizes[hash_sizes.size() - 1].hash_value != prv_hash) {
			hash_sizes.push_back((minimizer){.hash_value = prv_hash, .offset = cnt});
			cnt += hash_entries;
		}

        write_prog.update(((float)cnt/(float)total_entries) * 100, "Writing Sketch ");

        if (write_buf_size > 0) {
            fout.write((char *)&write_buf[0], write_buf_size * sizeof(Location));
        }

        uint64_t hashes = hash_sizes.size();
        fout.write((char*)&hashes, sizeof(uint64_t));
        fout.write((char*)&hash_sizes[0], hashes * sizeof(minimizer));
        free(write_buf);
    }

    write_prog.update(((float)cnt/(float)total_entries) * 100, "Writing Sketch ");

    fout.close();

    //Write sequences
    ofstream seqout(dp_path + "/" + file_name + ".sq.idx", ios::out | ios::binary);
    for (uint32_t i = 0; i < sequences.size(); i++) {
        uint8_t size = sequences[i].first.size();
        seqout.write((char*)&size, sizeof(uint8_t));
        seqout.write(&sequences[i].first[0], size);
        seqout.write((char*)&sequences[i].second, sizeof(offset_t));
    }
    seqout.close();
}

void Sketch::load() {
    uint64_t total_entries, total_hashes;

	ifstream mt_fin(dp_path + "/" + file_name + ".mt.idx", ios::in | ios::binary);
    mt_fin.read(reinterpret_cast<char*>(&kmer_size), sizeof(uint64_t));
    mt_fin.read(reinterpret_cast<char*>(&window_size), sizeof(uint64_t));
    mt_fin.read(reinterpret_cast<char*>(&total_entries), sizeof(uint64_t));
	mt_fin.close();

	
	//Read all locations
    ref_minimizers.resize(total_entries);
	ifstream fp_fin(dp_path + "/" + file_name + ".fp.idx", ios::in | ios::binary);
    fp_fin.read(reinterpret_cast<char*>(&ref_minimizers[0]), total_entries * sizeof(Location));
	fp_fin.close();

    //Read all hashes and sizes
	ifstream hv_fin(dp_path + "/" + file_name + ".hv.idx", ios::in | ios::binary);
    hv_fin.read(reinterpret_cast<char*>(&total_hashes), sizeof(uint64_t));
    minimizers.resize(total_hashes);
    hv_fin.read(reinterpret_cast<char*>(&minimizers[0]), total_hashes * sizeof(minimizer));
	hv_fin.close();

    string seq_file = dp_path + "/" + file_name + ".sq.idx";
    Logger::instance().info("Loading sequence names from: %s\n", seq_file.c_str());
    ifstream seqin(seq_file, ios::in | ios::binary);
    if (!seqin) {
        Logger::instance().info("Could not load file. Quitting.\n");
        exit(1);
    }

    string name;
    offset_t len;
    uint8_t name_size;
    while(seqin.read(reinterpret_cast<char*>(&name_size), sizeof(uint8_t))) {
        name.resize(name_size);
        seqin.read(&name[0], name_size);
        seqin.read(reinterpret_cast<char*>(&len), sizeof(offset_t));
        sequences.push_back({name, len});
    }
    seqin.close();
}

pair<vector<cut>, int> Sketch::query(vector<string> &reads, bool long_insertion, string& ref_l, string& ref_r) {
    pair<vector<cut>, int> cut_results = find_cuts_all(ref_l, ref_r, long_insertion, reads);
    return cut_results;
}

void Sketch::read_buffer() {
    buff_size = gzread(gz_fin, zbuffer, BUFFSIZE);
    size_read = gzoffset(gz_fin);
    buff_pos = 0;
    if (buff_size != 0) {
        if (PASS) {
            sketch_progress.update(((float)size_read/(float)file_size) * 100, "Building Sketch");
        }
        else {
            stats_progress.update(((float)size_read/(float)file_size) * 100, "Getting Stats");
        }
    }
    if (size_read == file_size) {
        if (PASS) {
            sort_progress.update(0.0, "Sorting Entries");
        }
//        else {
//            freq_progress.update(0.0, "Finding Freqs");
//        }
    }
    if (buff_size == 0 and gzeof(gz_fin) == 0) {
        buff_size = -1;
    }
    if (buff_size < 0) {
        int err;
        fprintf(stderr, "gzread error: %s\n", gzerror(gz_fin, &err));
        exit(1);
    }
}

inline uint32_t Sketch::read_line(string& seq) {
    char cur;

    uint32_t i = 0;
    while (true) {
        if (buff_pos >= buff_size) {
            read_buffer();
            if (buff_size == 0)
                return 0;
        }

        cur = zbuffer[buff_pos++];
        if (cur == '\n') {
            return i;
        }

        seq += cur;
        i++;
    }
}

void Sketch::dump_discard_set() {
    ofstream fout(dp_path + "/" + file_name + ".fq.idx", ios::out | ios::binary);
    uint32_t size = discard_hashes.size();
    fout.write((char*)&size, sizeof(uint32_t));
    unordered_set<hash_t>::iterator itr;
   
    for (itr = discard_hashes.begin(); itr != discard_hashes.end(); itr++) {
        fout.write((char*)&(*itr), sizeof(hash_t));
    }

    fout.close();
}

void Sketch::load_discard_set() {
    ifstream fin(dp_path + "/" + file_name + ".fq.idx", ios::in | ios::binary);
    uint32_t size;
    hash_t tmp;
    fin.read(reinterpret_cast<char*>(&size), sizeof(uint32_t));
    for (auto i = 0; i < size; i++) {
        fin.read(reinterpret_cast<char*>(&tmp), sizeof(hash_t));
        discard_hashes.insert(tmp);
    }
    
	fin.close();
}

void Sketch::merge_thread_files(int thread_id, int file_id, int& file_no) {
    if (file_no == 1) {
        string tmp = tmp_file_name + to_string(file_id) + "-0" + ".fp.idx";
        string out = tmp_file_name + to_string(tmp_file_no) + "-" + to_string(thread_id) + ".fp.idx";
        rename(tmp.c_str(), out.c_str());
        return;
    }

    while (file_no != 1) {
        if (file_no % 2 == 1) {
            string a = tmp_file_name + to_string(file_id) + "-" + to_string(file_no - 2) + ".fp.idx";
            string b = tmp_file_name + to_string(file_id) + "-" + to_string(file_no - 1) + ".fp.idx";
            string out = tmp_file_name + to_string(file_id) + "-" + to_string(file_no - 2) + ".fp.idx";
            merge(a, b, out); 
            file_no--;
        }
        for (int i = 0; i < file_no/2; i++) {
            string a = tmp_file_name + to_string(file_id) + "-" + to_string(2 * i) + ".fp.idx";
            string b = tmp_file_name + to_string(file_id) + "-" + to_string(2 * i + 1) + ".fp.idx";
            string out = tmp_file_name + to_string(file_id) + "-" + to_string(i) + ".fp.idx";
            merge(a, b, out); 
        }
        file_no /= 2;
    }
    string tmp = tmp_file_name + to_string(file_id) + "-0" + ".fp.idx";
    string out = tmp_file_name + to_string(tmp_file_no) + "-" + to_string(thread_id) + ".fp.idx";
	rename(tmp.c_str(), out.c_str());
 }

void Sketch::build_sketch_mt(int id, const ProgressBar progress, unordered_map<hash_t, hash_size_t> &high_freq_hashes) {
    string name, read;
    name.reserve(100);
    read.reserve(100000);
    int curr_id;
    int file_no = 0;
    int file_id = rand();

    vector<pair<hash_t, Location> > new_vec;
    new_vec.reserve(MERGE_BUFF);
    while (true) {
        // MUTEX
        read_mutex.lock();
        name.clear();
        read.clear();
        read_line(name);
        if (!name.size()) {
            read_mutex.unlock();
            break;
        }
        read_line(read);
        sequences.push_back({name.substr(1), read.size()});
        curr_id = read_id;
        read_id++;
        read_mutex.unlock();
        // MUTEX END

        transform(read.begin(), read.end(), read.begin(), ::toupper);
        get_ref_minimizers(&read[0], curr_id, read.size(), new_vec, high_freq_hashes);

        read.clear();
        name.clear();

        if (new_vec.size() >= MERGE_BUFF) {
            sort(new_vec.begin(), new_vec.end(), [](const auto &a, const auto &b) { return a.first < b.first;});
            ofstream fout(tmp_file_name + to_string(file_id) + "-" + to_string(file_no) + ".fp.idx", ios::out | ios::binary);
            fout.write((char*)&new_vec[0], new_vec.size() * sizeof(pair<hash_t, Location>));
            fout.close();        
            new_vec.clear();
            new_vec.reserve(MERGE_BUFF);
            file_no += 1;
        }
    }

    if (new_vec.size() != 0) {
        sort(new_vec.begin(), new_vec.end(), [](const auto &a, const auto &b) { return a.first < b.first;});
        ofstream fout(tmp_file_name + to_string(file_id) + "-" + to_string(file_no) + ".fp.idx", ios::out | ios::binary);
        fout.write((char*)&new_vec[0], new_vec.size() * sizeof(pair<hash_t, Location>));
        fout.close();        
        new_vec.clear();
        file_no += 1;
    }

	merge_thread_files(id, file_id, file_no);
}

void Sketch::build_sketch() {
    sketch_progress.update(0.0, "Building Sketch");

    unordered_map<hash_t, hash_size_t> sizes[thread_cnt];

    thread myThreads[thread_cnt];
    for (int i = 0; i < thread_cnt; i++){
        myThreads[i] = thread(&Sketch::build_sketch_mt, this, i, sketch_progress, ref(sizes[i]));
    }
    for (int i = 0; i < thread_cnt; i++){
        myThreads[i].join();
    }

    // Create discard set
    thread freq_merge_threads[thread_cnt/2];
    int mx_th = thread_cnt/2;
    while (mx_th > 0) {
        for (int i = 0; i < mx_th; i++) {
            freq_merge_threads[i] = thread(&Sketch::merge_sizes, this, ref(sizes[i]), ref(sizes[mx_th*2 - i - 1]));
        }
        for (int i = 0; i < mx_th; i++){
            freq_merge_threads[i].join();
        }
        freq_progress.update((1.0/(float)mx_th) * 100, "Getting Freqs");
        mx_th /= 2;
    }

    vector<pair<hash_t, hash_size_t>> hash_freqs;
    for (auto &it : sizes[0]) {
        hash_freqs.push_back({it.first, it.second});
    }
    sort(hash_freqs.begin(), hash_freqs.end(), [](const auto &a, const auto &b) { return a.second > b.second;});
    uint64_t top = hash_freqs.size() * DISCARD_FREQ;
    freq_th = hash_freqs[top].second;

    for (auto i = 0; i < hash_freqs.size(); i++) {
        if (hash_freqs[i].second >= freq_th) {
            discard_hashes.insert(hash_freqs[i].first);
        }
        else if (hash_freqs[i].second == 1) {
            discard_hashes.insert(hash_freqs[i].first);
        }
        else {
            total_entries += hash_freqs[i].second;
			total_hashes += 1;
        }
    }

    // dump_discard_set();

    // Merge
    thread merge_threads[thread_cnt/2];
    mx_th = thread_cnt/2;
    ProgressBar merge_prog(80);
    merge_prog.update(0.0, "Merging Entries");
	
    if (thread_cnt == 1) {
        merge_prog.update((1.0) * 100, "Merging Entries");
    }
	
    while (mx_th > 1) {
        for (int i = 0; i < mx_th; i++) {
            string a = tmp_file_name + to_string(tmp_file_no) + "-" + to_string(i) + ".fp.idx";
            string b = tmp_file_name + to_string(tmp_file_no) + "-" + to_string(mx_th*2 - i - 1) + ".fp.idx";
            string out = tmp_file_name + to_string(tmp_file_no + 1) + "-" + to_string(i) + ".fp.idx";
			merge_threads[i] = thread(&Sketch::merge, this, a, b, out);
        }
       	for (int i = 0; i < mx_th; i++){
			merge_threads[i].join();
       	}
		
        merge_prog.update((1.0/(float)mx_th) * 100, "Merging Entries");
        mx_th /= 2;
        tmp_file_no++;
    }
	
	string a = tmp_file_name + to_string(tmp_file_no) + "-0" + ".fp.idx";
	string b = tmp_file_name + to_string(tmp_file_no) + "-1" + ".fp.idx";
	merge_and_dump(a, b, discard_hashes);
	merge_prog.update((1.0) * 100, "Merging Entries");
}

void Sketch::merge_sizes(unordered_map<hash_t, hash_size_t> &a, unordered_map<hash_t, hash_size_t> &b) {
    for (auto &it : b) {
        if (a.find(it.first) != a.end()) {
            a[it.first] += it.second;
        }
        else {
            a[it.first] = it.second;
        }
    }
    b.erase(b.begin(), b.end());
}

void Sketch::fill_vector(vector<pair<hash_t, Location> >& v, uint64_t v_size, uint64_t& vptr, int& vrem, ifstream& vfin,
                         bool& done) {
    int v_read_size;

    if (v_size - vptr < MERGE_BUFF) {
        v_read_size = v_size - vptr;
    }
    else {
        v_read_size = MERGE_BUFF - vrem;
    }

    if (v_read_size == 0) {
		v.erase(v.begin(), v.end() - vrem);
        return;
    }

    v.erase(v.begin(), v.end() - vrem);
    vptr += v_read_size;

    vector<pair<hash_t, Location> > tmp;
    tmp.resize(v_read_size);
    vfin.read(reinterpret_cast<char*>(&tmp[0]), v_read_size * sizeof(pair<hash_t, Location>));
    v.reserve(v_read_size + vrem);

    v.insert(v.end(), std::make_move_iterator(tmp.begin()), std::make_move_iterator(tmp.end()));

    vrem = 0;
    done = vptr < v_size ? false : true;
}

void Sketch::merge_vectors(vector<pair<hash_t, Location> >& avec, vector<pair<hash_t, Location> >& bvec, int& arem, int& brem,
                           bool adone, bool bdone, ofstream& fout) {
    if (adone && bdone) {
        vector<pair<hash_t, Location> > merge_vec;
        merge_vec.reserve(avec.size() + bvec.size());
        std::merge(avec.begin(), avec.end(), bvec.begin(), bvec.end(), std::back_inserter(merge_vec),
                   [](const auto &a, const auto &b) { return a.first < b.first;});
        fout.write((char*)&merge_vec[0], merge_vec.size() * sizeof(pair<hash_t, Location>));
        return;
    }
    if (avec.empty()) {
        fout.write((char*)&bvec[0], bvec.size() * sizeof(pair<hash_t, Location>));
        brem = 0;
        return;
    }
    if (bvec.empty()) {
        fout.write((char*)&avec[0], avec.size() * sizeof(pair<hash_t, Location>));
        arem = 0;
        return;
    }

    hash_t amax, bmax, min_maxhash;
    int arem_tmp, brem_tmp, aend_tmp, bend_tmp;
    int bigger_end, bigger_rem, smaller_end, smaller_rem;

    if (adone) {
        aend_tmp = avec.size() ;
        arem_tmp = 0;
        amax = avec[avec.size() - 1].first;
    }
    else {
        hash_t alast = avec[avec.size() - 1].first;
		amax = alast;
		aend_tmp = avec.size();
		arem_tmp = 0;
        for (auto i = avec.size() - 2; i >= 0; i--) {
            if (avec[i].first != alast) {
                amax = avec[i].first;
                arem_tmp = avec.size() - i - 1;
                aend_tmp = i + 1;
                break;
            }
        }
    }

    if (bdone) {
        bend_tmp = bvec.size();
        brem_tmp = 0;
        bmax = bvec[bvec.size() - 1].first;
    }
    else {
        hash_t blast = bvec[bvec.size() - 1].first;
		bmax = blast;
		bend_tmp = bvec.size();
		brem_tmp = 0;
        for (auto i = bvec.size() - 2; i >= 0; i--) {
            if (bvec[i].first != blast) {
                bmax = bvec[i].first;
                bend_tmp = i + 1;
                brem_tmp = bvec.size() - i - 1;
                break;
            }
        }
    }

    bool smaller_done, bigger_done;
    vector<pair<hash_t, Location> > bigger, smaller;
    if (amax <= bmax) {
        min_maxhash = amax;
        smaller = avec;
        smaller_done = adone;
        smaller_end = aend_tmp;
        smaller_rem = arem_tmp;
        bigger = bvec;
    }
    else {
        min_maxhash = bmax;
        smaller = bvec;
        smaller_done = bdone;
        smaller_end = bend_tmp;
        smaller_rem = brem_tmp;
        bigger = avec;
    }

    auto upper = upper_bound(bigger.begin(), bigger.end(), min_maxhash, [](const auto &a, const auto &b) { return a < b.first;});
    bigger_end = upper - bigger.begin();
    bigger_rem = bigger.size() - bigger_end;

    vector<pair<hash_t, Location> > merge_vec;
    merge_vec.reserve(smaller.size() + bigger.size());
    std::merge(smaller.begin(), smaller.begin() + smaller_end, bigger.begin(), bigger.begin() + bigger_end, std::back_inserter(merge_vec),
               [](const auto &a, const auto &b) { return a.first < b.first;});
    fout.write((char*)&merge_vec[0], merge_vec.size() * sizeof(pair<hash_t, Location>));
	merge_vec.clear();

    if (amax <= bmax) {
        brem = bigger_rem;
        arem = smaller_rem;
    }
    else {
        brem = smaller_rem;
        arem = bigger_rem;
    }
}

void Sketch::merge_and_dump_vectors(vector<pair<hash_t, Location> >& avec, vector<pair<hash_t, Location> >& bvec, int& arem, int& brem,
							bool adone, bool bdone, ofstream& fout, unordered_set<hash_t>& discard_set, vector<minimizer>& hash_sizes, 
							hash_t& prv_hash, uint64_t& hash_entries, uint64_t& cnt) {
    if (adone && bdone) {
        vector<pair<hash_t, Location> > merge_vec;
        merge_vec.reserve(avec.size() + bvec.size());
        std::merge(avec.begin(), avec.end(), bvec.begin(), bvec.end(), std::back_inserter(merge_vec),
                   [](const auto &a, const auto &b) { return a.first < b.first;});
        dump_vector(merge_vec, discard_set, hash_sizes, prv_hash, hash_entries, cnt, fout);;
        return;
    }
    if (avec.empty()) {
        dump_vector(bvec, discard_set, hash_sizes, prv_hash, hash_entries, cnt, fout);
        brem = 0;
        return;
    }
    if (bvec.empty()) {
        dump_vector(avec, discard_set, hash_sizes, prv_hash, hash_entries, cnt, fout);
        arem = 0;
        return;
    }

    hash_t amax, bmax, min_maxhash;
    int arem_tmp, brem_tmp, aend_tmp, bend_tmp;
    int bigger_end, bigger_rem, smaller_end, smaller_rem;

    if (adone) {
        aend_tmp = avec.size();
        arem_tmp = 0;
        amax = avec[avec.size() - 1].first;
    }
    else {
        hash_t alast = avec[avec.size() - 1].first;
		aend_tmp = avec.size();
		arem_tmp = 0;
        for (auto i = avec.size() - 2; i >= 0; i--) {
            if (avec[i].first != alast) {
                amax = avec[i].first;
                arem_tmp = avec.size() - i - 1;
                aend_tmp = i + 1;
                break;
            }
        }
    }

    if (bdone) {
        bend_tmp = bvec.size();
        brem_tmp = 0;
        bmax = bvec[bvec.size() - 1].first;
    }
    else {
        hash_t blast = bvec[bvec.size() - 1].first;
		bmax = blast;
		bend_tmp = bvec.size();
		brem_tmp = 0;
        for (auto i = bvec.size() - 2; i >= 0; i--) {
            if (bvec[i].first != blast) {
                bmax = bvec[i].first;
                bend_tmp = i + 1;
                brem_tmp = bvec.size() - i - 1;
                break;
            }
        }
    }

    bool smaller_done, bigger_done;
    vector<pair<hash_t, Location> > bigger, smaller;
    if (amax <= bmax) {
        min_maxhash = amax;
        smaller = avec;
        smaller_done = adone;
        smaller_end = aend_tmp;
        smaller_rem = arem_tmp;
        bigger = bvec;
    }
    else {
        min_maxhash = bmax;
        smaller = bvec;
        smaller_done = bdone;
        smaller_end = bend_tmp;
        smaller_rem = brem_tmp;
        bigger = avec;
    }

    auto upper = upper_bound(bigger.begin(), bigger.end(), min_maxhash, [](const auto &a, const auto &b) { return a < b.first;});
    bigger_end = upper - bigger.begin();
    bigger_rem = bigger.size() - bigger_end;

    vector<pair<hash_t, Location> > merge_vec;
    merge_vec.reserve(smaller.size() + bigger.size());
    std::merge(smaller.begin(), smaller.begin() + smaller_end, bigger.begin(), bigger.begin() + bigger_end, std::back_inserter(merge_vec),
               [](const auto &a, const auto &b) { return a.first < b.first;});
	dump_vector(merge_vec, discard_set, hash_sizes, prv_hash, hash_entries, cnt, fout);

    if (amax <= bmax) {
        brem = bigger_rem;
        arem = smaller_rem;
    }
    else {
        brem = smaller_rem;
        arem = bigger_rem;
    }
}

void Sketch::cleanup_vectors(vector<pair<hash_t, Location> >& avec, vector<pair<hash_t, Location> >& bvec, int& arem, int& brem,
                             ofstream& fout) {
    avec.erase(avec.begin(), avec.end() - arem);
    bvec.erase(bvec.begin(), bvec.end() - brem);
    vector<pair<hash_t, Location> > merge_vec;
    merge_vec.reserve(avec.size() + bvec.size());
    std::merge(avec.begin(), avec.end(), bvec.begin(), bvec.end(), std::back_inserter(merge_vec),
               [](const auto &a, const auto &b) { return a.first < b.first;});
    fout.write((char*)&merge_vec[0], merge_vec.size() * sizeof(pair<hash_t, Location>));
}

void Sketch::merge(string file_a, string file_b, string output) {
    uint64_t a_size, b_size;
    bool adone = false, bdone = false;
    ifstream asfin(file_a, ios::ate | ios::binary);
    a_size = asfin.tellg()/sizeof(pair<hash_t, Location>);
    ifstream bsfin(file_b, ios::ate | ios::binary);
    b_size = bsfin.tellg()/sizeof(pair<hash_t, Location>);
    ifstream afin(file_a, ios::in | ios::binary);
    ifstream bfin(file_b, ios::in | ios::binary);
	int r = rand();
    string tmp_output = dp_path + "/temp-" + to_string(r) + ".fp.idx";
    ofstream fout(tmp_output, ios::out | ios::binary);

    vector<pair<hash_t, Location> > avec;
    vector<pair<hash_t, Location> > bvec;
    uint64_t aptr = 0, bptr = 0;
    int arem = 0, brem = 0;

    while (!adone || !bdone) {
        fill_vector(avec, a_size, aptr, arem, afin, adone);
        fill_vector(bvec, b_size, bptr, brem, bfin, bdone);
        merge_vectors(avec, bvec, arem, brem, adone, bdone, fout);
    }
    
	avec.clear();
	bvec.clear(); 
    fout.close();
	remove(file_a.c_str());
	remove(file_b.c_str());
    rename(tmp_output.c_str(), output.c_str());
}

void Sketch::merge_and_dump(string file_a, string file_b, unordered_set<hash_t>& discard_set) {
    uint64_t a_size, b_size;
    bool adone = false, bdone = false;
    ifstream asfin(file_a, ios::ate | ios::binary);
    a_size = asfin.tellg()/sizeof(pair<hash_t, Location>);
    ifstream bsfin(file_b, ios::ate | ios::binary);
    b_size = bsfin.tellg()/sizeof(pair<hash_t, Location>);
    ifstream afin(file_a, ios::in | ios::binary);
    ifstream bfin(file_b, ios::in | ios::binary);
	int r = rand();

	ofstream mt_fout(dp_path + "/" + file_name + ".mt.idx", ios::out | ios::binary);
	mt_fout.write((char*)&kmer_size, sizeof(uint64_t));
    mt_fout.write((char*)&window_size, sizeof(uint64_t));
    mt_fout.write((char*)&total_entries, sizeof(uint64_t));
	mt_fout.close();

	ofstream fp_fout(dp_path + "/" + file_name + ".fp.idx", ios::out | ios::binary);
	uint64_t cnt = 0, hash_entries = 0;
	vector<minimizer> hash_sizes;
	hash_t prv_hash = MAX_HASH;

    vector<pair<hash_t, Location> > avec;
    vector<pair<hash_t, Location> > bvec;
    uint64_t aptr = 0, bptr = 0;
    int arem = 0, brem = 0;

    while (!adone || !bdone) {
        fill_vector(avec, a_size, aptr, arem, afin, adone);
        fill_vector(bvec, b_size, bptr, brem, bfin, bdone);
		merge_and_dump_vectors(avec, bvec, arem, brem, adone, bdone, fp_fout, discard_set, hash_sizes, 
		 						prv_hash, hash_entries, cnt);
    }
    
	avec.clear();
	bvec.clear(); 
    fp_fout.close();
    remove(file_a.c_str());
    remove(file_b.c_str());

	uint64_t hashes = hash_sizes.size();
	ofstream hv_fout(dp_path + "/" + file_name + ".hv.idx", ios::out | ios::binary);
	hv_fout.write((char*)&hashes, sizeof(uint64_t));
	hv_fout.write((char*)&hash_sizes[0], hashes * sizeof(minimizer));
	hv_fout.close();

	ofstream seqout(dp_path + "/" + file_name + ".sq.idx", ios::out | ios::binary);
    for (uint32_t i = 0; i < sequences.size(); i++) {
        uint8_t size = sequences[i].first.size();
        seqout.write((char*)&size, sizeof(uint8_t));
        seqout.write(&sequences[i].first[0], size);
        seqout.write((char*)&sequences[i].second, sizeof(offset_t));
    }
    seqout.close();
}

void Sketch::dump_vector(vector<pair<hash_t, Location> >& vec, unordered_set<hash_t> &discard_set, vector<minimizer>& hash_sizes, 
						hash_t& prv_hash, uint64_t& hash_entries, uint64_t& cnt, ofstream& fout) {
    // ProgressBar write_prog(80);
    // write_prog.update(0.0, "Writing Sketch ");

    int write_buf_size = 0;
    Location* write_buf = (Location*)malloc(1000000 * sizeof(Location));

    if (!vec.empty()) {
        uint64_t i = 0;

		if (prv_hash == MAX_HASH) {
			while (discard_set.find(vec[i].first) != discard_set.end()) {
            	i++;
        	}
			if (i < vec.size()) {
				write_buf[write_buf_size++] = vec[i].second;
        		prv_hash = vec[i].first;
        		hash_entries = 1;
				i++;
			}
			else {
				return;
			}
		}

        for (; i < vec.size(); i++) {
            if (discard_set.find(vec[i].first) != discard_set.end()) {
                continue;
            }
            if (vec[i].first == prv_hash)
                hash_entries++;
            else {
                hash_sizes.push_back((minimizer){.hash_value = prv_hash, .offset = cnt});
                cnt += hash_entries;
                prv_hash = vec[i].first;
                hash_entries = 1;
            }
            if (write_buf_size == 1000000) {
                fout.write((char*)&write_buf[0], 1000000 * sizeof(Location));
                write_buf_size = 0;
                // write_prog.update(((float)cnt/(float)total_entries) * 100, "Writing Sketch ");
            }
            write_buf[write_buf_size++] = vec[i].second;
        }

        // write_prog.update(((float)cnt/(float)total_entries) * 100, "Writing Sketch ");

		if ((hash_sizes.empty() && prv_hash != MAX_HASH) || (!hash_sizes.empty() && hash_sizes[hash_sizes.size() - 1].hash_value != prv_hash)) {
			hash_sizes.push_back((minimizer){.hash_value = prv_hash, .offset = cnt});
			cnt += hash_entries;
			prv_hash = MAX_HASH;
			hash_entries = 0;
		}

        if (write_buf_size > 0) {
            fout.write((char *)&write_buf[0], write_buf_size * sizeof(Location));
        }

        free(write_buf);
    }

    // write_prog.update(((float)cnt/(float)total_entries) * 100, "Writing Sketch ");
}

void Sketch::get_ref_minimizers(char* read, read_id_t id, int len, vector<pair<hash_t, Location> > &ref_minimizers_vec,
                                unordered_map<hash_t, hash_size_t> &hashes) {
    deque<pair<hash_t, Location> > window;

    char tmp[hash_bytes];

    int win = window_size - 1;
    for (int i = 0; i < len - kmer_size + 1; i++) {

        MurmurHash3_x86_32(read + i, kmer_size, 50, tmp);
        hash_t hash_fw = *((hash_t*)tmp);

        Location loc;
        loc.seq_id = id;
        loc.offset = i;

        while(!window.empty() && window.back().first >= hash_fw)
            window.pop_back();

        window.push_back(pair<hash_t, Location>(hash_fw, loc));

        while (!window.empty() && window.front().second.offset <= i - window_size)
            window.pop_front();

        if (i >= win) {
            if (ref_minimizers_vec.empty() || ref_minimizers_vec.back().first != window.front().first) {
                auto minimizer = window.front();
                if (hashes.find(minimizer.first) != hashes.end()) {
                    hashes[minimizer.first] += 1;
                }
                else {
                    hashes[minimizer.first] = 1;
                }
                ref_minimizers_vec.push_back(window.front());
            }
        }
    }
}

void Sketch::get_query_minimizers(char* read, read_id_t id, offset_t len, vector<minimizer_t> &minimizers_vec) {
    deque<minimizer_t> window;

    char tmp[hash_bytes];

    int win = window_size - 1;
    for (auto i = 0; i < len - kmer_size + 1; i++) {

        MurmurHash3_x86_32(read + i, kmer_size, 50, tmp);
        hash_t mhash = *((hash_t *)tmp);

        while(!window.empty() && window.back().first >= mhash)
            window.pop_back();

        window.push_back(minimizer_t(mhash, i));

        while (!window.empty() && window.front().second <= i - window_size)
            window.pop_front();

        if (i >= win) {
            if (minimizers_vec.empty() || minimizers_vec.back().first != window.front().first) {
                minimizers_vec.push_back(window.front());
            }
        }
    }
}

void Sketch::compute_freq_th() {
    vector<hash_size_t> frequencies;
    hash_t prv_offset = 0;
    for (auto it = minimizers.begin() + 1; it != minimizers.end(); it++) {
        hash_size_t frq = it->offset - prv_offset;
        prv_offset = it->offset;
        frequencies.push_back(frq);
    }
    frequencies.push_back(ref_minimizers.size() - prv_offset);
    sort(frequencies.begin(), frequencies.end(), [](auto &a, auto &b) {
        return a > b;
    });
    hash_size_t top = minimizers.size() * 0.01;
    freq_th = frequencies[top];
}

pair<mem_offset_t, hash_size_t> Sketch::find_hit(const hash_t &hv) {
    vector<minimizer>::iterator it = lower_bound(minimizers.begin(), minimizers.end(), hv, minimizer());

    if (it == minimizers.end()) {
        return {0, 0};
    }
    else if (it->hash_value == hv) {
        mem_offset_t offset = it->offset;
        hash_size_t size = 0;
        if (it+1 != minimizers.end()) {
            size = (it+1)->offset - it->offset;
        }
        else {
            size = ref_minimizers.size() - it->offset;
        }
        return {offset, size};
    }
    else {
        return {0, 0};
    }
};

vector<hit> Sketch::get_hits(vector<minimizer_t>& query_frw) {
    vector<hit> hits_frw;
    vector<minimizer_t> hashes;
    Logger::instance().debug("Getting hits\n");
    hash_t prv_hash = MAX_HASH;
    for (auto it = query_frw.begin(); it != query_frw.end(); it++) {
        // if (discard_hashes.find(it->first) != discard_hashes.end()) {
        //     continue;
        // }
        auto idx = find_hit(it->first);
        Logger::instance().debug("Hash = %lld | size = %d\n", it->first, idx.second);
//        if (idx.second == 0 || idx.second >= freq_th) {
        if (idx.second == 0) {
            continue;
        }

        hashes.push_back(*it);

//        if (it->first != prv_hash) {
        for (auto i = idx.first; i < idx.first + idx.second; i++) {
            hits_frw.push_back((hit) {.seq_id = ref_minimizers[i].seq_id, .hash_value = it->first,
                    .offset = ref_minimizers[i].offset, .genome_offset = (offset_t)it->second});
        }
        prv_hash = it->first;
    }
    sort(hits_frw.begin(), hits_frw.end());

    query_frw = hashes;
    hashes.clear();

    return hits_frw;
}

void Sketch::get_genome_hits(string& ref, vector<minimizer_t>& minimizers, vector<hit>& candidates) {
    transform(ref.begin(), ref.end(), ref.begin(), ::toupper);
    get_query_minimizers(&ref[0], 0, ref.size(), minimizers);
    Logger::instance().debug("Query minimizers size = %d\n", minimizers.size());
    sort(minimizers.begin(), minimizers.end());

    candidates = get_hits(minimizers);
}

vector<seed> Sketch::create_seeds(vector<hit>& hits, int start, int size) {
    vector<seed> seeds;

    for (auto i = start; i < start + size; i++) {
        seed curr_seed;
        curr_seed.qPos = hits[i].offset;
        curr_seed.sPos = hits[i].genome_offset;
        curr_seed.len = kmer_size;
        seeds.push_back(curr_seed);
    }

    return seeds;
}

MaxChainInfo Sketch::get_genome_anchor(vector<hit>& left_anchor_hits, int start, int size) {
    vector<seed> lseeds = create_seeds(left_anchor_hits, start, size);
    MaxChainInfo max_chain_l = claspChain.get_max_chain(lseeds);
    int number_of_minimizers = (max_chain_l.gaps_size + max_chain_l.score)/kmer_size;
    Logger::instance().debug("C: (Q: %d-%d, G: %d-%d, L: %d, S: %f, K: %d) | ", max_chain_l.qrange.first,
                             max_chain_l.qrange.second, max_chain_l.rrange.first, max_chain_l.rrange.second, max_chain_l.len,
                             max_chain_l.score, number_of_minimizers);

    return max_chain_l;
}

inline int Sketch::max_kmer_count(MaxChainInfo chain, int anchor_length, int read_length, int genome_minimizer_count,
                                  vector<minimizer_t>& genome_fingerprint) {
    int qdistance = chain.qrange.second - chain.qrange.first;
    int gdistance = chain.rrange.second - chain.rrange.first;
    float read_distance_coefficient = (float)qdistance/(float)gdistance;
    int p_left = 1;
    if (chain.qrange.first < chain.rrange.first * read_distance_coefficient) {
        p_left = ((chain.rrange.first - chain.qrange.first) / read_distance_coefficient);
    }
    int p_right = anchor_length;
    if (read_length - chain.qrange.second < (anchor_length - chain.rrange.second) * read_distance_coefficient) {
        p_right = chain.rrange.second + ((read_length - chain.qrange.second)/read_distance_coefficient);
    }

    Logger::instance().debug("PL: %d | PR: %d | RL: %d | ", p_left, p_right, read_length);

    int minimizer_count = 0;
    int start = -kmer_size;
    for (int i = 0; i < genome_fingerprint.size(); i++) {
        if (genome_fingerprint[i].second < p_left)
            continue;
        if (genome_fingerprint[i].second > p_right)
            break;
        if (genome_fingerprint[i].second < start + kmer_size - 1)
            continue;
        else {
            minimizer_count++;
            start = genome_fingerprint[i].second;
        }
    }

    return minimizer_count;
}

void Sketch::find_left_cuts(vector<hit>& read_candidates, vector<cut>& candidates_cut_info, int anchor_length, orientation_en orientation,
                            int genome_minimizers_cnt, unordered_set<read_id_t>& insertion_candidates,
                            vector<minimizer_t>& genome_fingerprint, float ti) {
    Logger::instance().debug("&& Genome Minimizers: %d\n", genome_minimizers_cnt);
    Logger::instance().debug("&& TGA = %f\n", TGA * genome_minimizers_cnt);
    vector<int> left_reads_remaining_size, right_reads_remaining_size;

    sort(genome_fingerprint.begin(), genome_fingerprint.end(), [](const auto &a, const auto &b) { return a.second < b.second;});

    for (auto it = 0; it < read_candidates.size(); it++) {
        read_id_t curr_id = read_candidates[it].seq_id;

        auto curr_idx = it;

        auto upper = upper_bound(read_candidates.begin(), read_candidates.end(), curr_id, hit());
        hash_size_t size = upper - read_candidates.begin() - curr_idx;
        if (upper == read_candidates.end())
            size = read_candidates.size() - curr_idx;

        it += size;

        //t_ga
        int minimizer_cutoff = (int)((1.0 * GENOME_ANCHOR_CUTOFF/anchor_length) * genome_minimizers_cnt);
        if (size < TGA * genome_minimizers_cnt) {
            continue;
        }

        //t_i
        Logger::instance().debug("&& Minimizers: %d\n", genome_minimizers_cnt);
        if (check_insertion_content && ti != 0) {
            if (insertion_candidates.find(curr_id) == insertion_candidates.end()) {
                Logger::instance().debug("%-30s | Dropped by TI\n", sequences[curr_id].first.c_str());
                continue;
            }
        }

        Logger::instance().debug("%-30s: ", sequences[curr_id].first.c_str());
        MaxChainInfo max_chain = get_genome_anchor(read_candidates, curr_idx, size);
        Logger::instance().debug(" S: %d | ", size);

        int number_of_minimizers = (max_chain.gaps_size + max_chain.score)/kmer_size;
        int max_kmer = max_kmer_count(max_chain, anchor_length, (int)sequences[curr_id].second, genome_minimizers_cnt, genome_fingerprint);
        float ratio = (float)number_of_minimizers/(float)max_kmer;

        if (number_of_minimizers < TCK * max_kmer) {
            Logger::instance().debug("DROPPED(LM)\n");
            continue;
        }

//        if (anchor_length - max_chain.rrange.second > BOUNDARY_DISTANCE_CUTOFF) {
//            Logger::instance().debug("DROPPED(BR)\n");
//            continue;
//        }
        if (max_chain.qrange.first > BOUNDARY_DISTANCE_CUTOFF && max_chain.rrange.first > BOUNDARY_DISTANCE_CUTOFF) {
            Logger::instance().debug("DROPPED(QS)\n");
            continue;
        }
        if (max_chain.qrange.second - max_chain.qrange.first < GENOME_ANCHOR_CUTOFF) {
            Logger::instance().debug("DROPPED(QL)\n");
            continue;
        }
        int qdistance = max_chain.qrange.second - max_chain.qrange.first;
        int gdistance = max_chain.rrange.second - max_chain.rrange.first;
        float read_distance_coefficient = (float)qdistance/(float)gdistance;
        if (qdistance - gdistance > 0.15 * gdistance) {
            Logger::instance().debug("DROPPED(TH)\n");
            continue;
        }

        int pivot = max(anchor_length - GENOME_ANCHOR_LEN, max_chain.rrange.first);
        int skip = (pivot - max_chain.rrange.second) * read_distance_coefficient;

        if (max_chain.qrange.second + skip > (int)sequences[curr_id].second - 10) {
            Logger::instance().debug("DROPPED(NER)\n");
            continue;
        }

        cut read_cut_info;
        read_cut_info.seq_id = curr_id;
        read_cut_info.orientation = orientation;
        read_cut_info.range.start = max(0, max_chain.qrange.second + skip);
        read_cut_info.range.end = sequences[curr_id].second - 1;
        read_cut_info.genome_range.start = max_chain.rrange.first;
        read_cut_info.genome_range.end = pivot;
        read_cut_info.breakpoint_distance = abs(pivot - anchor_length);
        read_cut_info.number_of_kmers = size;

        Logger::instance().debug("Q: [%4d - %4d, %4d] | G: [%4d - %4d] | P: %d | ++\n", read_cut_info.range.start, read_cut_info.range.end,
                                 read_cut_info.range.end - read_cut_info.range.start, read_cut_info.genome_range.start,
                                 read_cut_info.genome_range.end, read_cut_info.breakpoint_distance);

        candidates_cut_info.push_back(read_cut_info);
    }
}

void Sketch::find_right_cuts(vector<hit>& read_candidates, vector<cut>& candidates_cut_info, int anchor_length, orientation_en orientation,
                             int genome_minimizers_cnt, unordered_set<read_id_t>& insertion_candidates,
                             vector<minimizer_t>& genome_fingerprint, float ti) {
    Logger::instance().debug("&& Genome Minimizers: %d\n", genome_minimizers_cnt);
    Logger::instance().debug("&& Insertion Minimizers: %d\n", genome_minimizers_cnt);
    vector<int> left_reads_remaining_size, right_reads_remaining_size;
    int sum = 0;
    int cnt = 0;

    sort(genome_fingerprint.begin(), genome_fingerprint.end(), [](const auto &a, const auto &b) { return a.second < b.second;});

    for (auto it = 0; it < read_candidates.size(); it++) {
        int begin = 0, end = 0;
        read_id_t curr_id = read_candidates[it].seq_id;

        auto curr_idx = it;

        auto upper = upper_bound(read_candidates.begin(), read_candidates.end(), curr_id, hit());
        hash_size_t size = upper - read_candidates.begin() - curr_idx;
        if (upper == read_candidates.end())
            size = read_candidates.size() - curr_idx;

        it += size;

        //t_ga
        int minimizer_cutoff = (int)((1.0 * GENOME_ANCHOR_CUTOFF/anchor_length) * genome_minimizers_cnt);
        if (size < TGA * genome_minimizers_cnt) {
            continue;
        }

        //t_i
        if (check_insertion_content && ti != 0) {
            if (insertion_candidates.find(curr_id) == insertion_candidates.end()) {
                continue;
            }
        }

        Logger::instance().debug("%-30s: ", sequences[curr_id].first.c_str());
        MaxChainInfo max_chain = get_genome_anchor(read_candidates, curr_idx, size);
        Logger::instance().debug(" S: %d | ", size);
        int qdistance = max_chain.qrange.second - max_chain.qrange.first;
        int gdistance = max_chain.rrange.second - max_chain.rrange.first;

        int number_of_minimizers = (max_chain.gaps_size + max_chain.score)/kmer_size;
        int max_kmer = max_kmer_count(max_chain, anchor_length, (int)sequences[curr_id].second, genome_minimizers_cnt, genome_fingerprint);
        float ratio = (float)number_of_minimizers/(float)max_kmer;

        if (number_of_minimizers < TCK * max_kmer) {
            Logger::instance().debug("DROPPED(LM)\n");
            continue;
        }

        if (sequences[curr_id].second - max_chain.qrange.second > BOUNDARY_DISTANCE_CUTOFF &&
            anchor_length - max_chain.rrange.second > BOUNDARY_DISTANCE_CUTOFF) {
            Logger::instance().debug("DROPPED(QE)\n");
            continue;
        }
        if (max_chain.qrange.second - max_chain.qrange.first < GENOME_ANCHOR_CUTOFF) {
            Logger::instance().debug("DROPPED(QL)\n");
            continue;
        }

        if (qdistance - gdistance > 0.15 * gdistance) {
            Logger::instance().debug("DROPPED(TH)\n");
            continue;
        }

        int pivot = min(GENOME_ANCHOR_LEN, max_chain.rrange.second);
        int skip = (pivot - max_chain.rrange.first) * (float)qdistance/(float)gdistance;

        if (max_chain.qrange.first + skip < 10) {
            Logger::instance().debug("DROPPED(NEL)\n");
            continue;
        }

        cut read_cut_info;
        read_cut_info.seq_id = curr_id;
        read_cut_info.orientation = orientation;
        read_cut_info.range.end = min(sequences[curr_id].second - 1, max(max_chain.qrange.first + skip, 0));
        read_cut_info.range.start = 0;
        read_cut_info.genome_range.start = pivot;
        read_cut_info.genome_range.end = max_chain.rrange.second;
        read_cut_info.breakpoint_distance = pivot;
        read_cut_info.number_of_kmers = size;

        Logger::instance().debug("Q: [%4d - %4d, %4d] | G: [%4d - %4d] | P: %d | **\n", read_cut_info.range.start, read_cut_info.range.end,
                                 read_cut_info.range.end - read_cut_info.range.start, read_cut_info.genome_range.start,
                                 read_cut_info.genome_range.end, read_cut_info.breakpoint_distance);

        candidates_cut_info.push_back(read_cut_info);
    }
}

void Sketch::merge_candidates(vector<cut>& left_candidates, vector<cut>& right_candidates, vector<cut>& merged_candidates,
                              cut_stats& stats, bool long_insertion) {
    int i = 0, j = 0;
    cut dummy;
    dummy.seq_id = INT_MAX;
    left_candidates.push_back(dummy);
    right_candidates.push_back(dummy);

    while (i < left_candidates.size() || j < right_candidates.size()) {
        if (left_candidates[i].seq_id < right_candidates[j].seq_id) {
            left_candidates[i].type = PARTIAL_LEFT;
            merged_candidates.push_back(left_candidates[i]);
            stats.left_cuts_size.push_back(left_candidates[i].range.end - left_candidates[i].range.start);
            i++;
        }
        else if (right_candidates[j].seq_id < left_candidates[i].seq_id) {
            right_candidates[j].type = PARTIAL_RIGHT;
            merged_candidates.push_back(right_candidates[j]);
            stats.right_cuts_size.push_back(right_candidates[j].range.end - right_candidates[j].range.start);
            j++;
        }
        else {
            if (left_candidates[i].seq_id == INT_MAX)
                break;
            if (left_candidates[i].range.start > right_candidates[j].range.end) {
                cut merged_cut = left_candidates[i];
                merged_cut.range.start = right_candidates[j].range.end;
                merged_cut.range.end = left_candidates[i].range.start;

                if (merged_cut.range.end - merged_cut.range.start < left_candidates[i].breakpoint_distance +
                                                                    right_candidates[j].breakpoint_distance) {
                    offset_t extension = ((left_candidates[i].breakpoint_distance + right_candidates[j]
                            .breakpoint_distance)
                                          - (merged_cut.range.end - merged_cut.range.start)) / 2;
                    merged_cut.range.start = max(0, merged_cut.range.start - extension);
                    merged_cut.range.end = min((int)sequences[merged_cut.seq_id].second, merged_cut.range.end + extension);
                }

                merged_cut.type = OVERLAPPING_READ;
                Logger::instance().debug("Setting Overlap\n");
                merged_cut.estimated_insertion = -1;
                merged_candidates.push_back(merged_cut);
                merged_cut.number_of_kmers = -1;
                stats.overlapping_cnt++;
                i++;
                j++;
            }
            else {
                cut merged_cut = left_candidates[i];
                merged_cut.range.end = right_candidates[j].range.end;
                merged_cut.estimated_insertion = abs(merged_cut.range.end - merged_cut.range.start -
                                                     (left_candidates[i].breakpoint_distance + right_candidates[j].breakpoint_distance));
                    offset_t extension = merged_cut.estimated_insertion / 2;
                    merged_cut.range.start = max(0, merged_cut.range.start - extension);
                    merged_cut.range.end = min((int)sequences[merged_cut.seq_id].second, merged_cut.range.end + extension);
                merged_cut.type = BIMODAL;
                merged_cut.number_of_kmers = right_candidates[j].number_of_kmers + left_candidates[i].number_of_kmers;
                i++;
                j++;
                if (!long_insertion && merged_cut.estimated_insertion < 10) {
                    Logger::instance().debug("DROPPED(%s, I=%d, R=%d-%d)\n", sequences[merged_cut.seq_id].first.c_str(),
                                             merged_cut.estimated_insertion, merged_cut.range.start, merged_cut.range.end);
                    continue;
                }
                stats.bimodal_sum += merged_cut.estimated_insertion;
                stats.bimodal_cnt++;
                merged_candidates.push_back(merged_cut);
            }
        }
    }
    sort(stats.left_cuts_size.begin(), stats.left_cuts_size.end());
    sort(stats.right_cuts_size.begin(), stats.right_cuts_size.end());
    Logger::instance().debug("%d\n", merged_candidates.size());
}

void Sketch::get_unique_minimizers(vector<string> &reads, unordered_set<hash_t>& insertion_minimizers) {
    vector<minimizer_t> minimizers_vec;
    for (int i = 0; i < reads.size(); i++) {
        transform(reads[i].begin(), reads[i].end(), reads[i].begin(), ::toupper);
        get_query_minimizers(&reads[i][0], i, reads[i].size(), minimizers_vec);
    }
    for (int i = 0; i < minimizers_vec.size(); i++) {
        insertion_minimizers.insert(minimizers_vec[i].first);
    }
}

void Sketch::get_insertion_minimizers(vector<string>& short_reads, string& genome_left, string& genome_right,
                                      unordered_set<read_id_t>& candidates) {
    unordered_set<hash_t> reads_minimizers;

    get_unique_minimizers(short_reads, reads_minimizers);
    Logger::instance().debug("-- SR Minimizers = %d\n", reads_minimizers.size());

    unordered_set<hash_t> genome_minimizers;

    transform(genome_left.begin(), genome_left.end(), genome_left.begin(), ::toupper);
    vector<string> genome_segments;
    genome_segments.push_back(genome_left);
    genome_segments.push_back(genome_right);

    get_unique_minimizers(genome_segments, genome_minimizers);
    Logger::instance().debug("-- Genome Minimizers = %d\n", genome_minimizers.size());

    vector<hit> candidates_vec;

    int insertion_minimizers_size = 0;

    unordered_map<read_id_t, int> read_count;

    read_count.reserve(100000);
    read_id_t prv_id = -1;
    for (auto it = reads_minimizers.begin(); it != reads_minimizers.end(); it++) {
        if (discard_hashes.find(*it) != discard_hashes.end()) {
            continue;
        }

        if (genome_minimizers.find(*it) == genome_minimizers.end()) {
            auto idx = find_hit(*it);
            Logger::instance().debug("%llu | %d\n", *it, idx.second);

//            if (idx.second == 0 || idx.second >= freq_th) {
            if (idx.second == 0) {
                continue;
            }

            insertion_minimizers_size++;

            sort(ref_minimizers.begin() + idx.first, ref_minimizers.begin() + idx.first + idx.second);
            for (auto i = idx.first; i < idx.first + idx.second; i++) {
                if (ref_minimizers[i].seq_id == prv_id) {
                    continue;
                }

                prv_id = ref_minimizers[i].seq_id;
                auto f = read_count.find(ref_minimizers[i].seq_id);
                if (f != read_count.end()) {
                    f->second++;
                }
                else {
                    read_count[ref_minimizers[i].seq_id] = 1;
                }
            }
        }
        prv_id = -1;
    }

    int cutoff = INSERTION_MINIMIZER_CUTOFF * insertion_minimizers_size;
    Logger::instance().debug("-- TI = %d\n", cutoff);
    for (auto it = read_count.begin(); it != read_count.end(); it++) {
        Logger::instance().debug("%-30s | %d\n", sequences[it->first].first.c_str(), it->second);
        if (it->second >= cutoff) {
            candidates.emplace_hint(candidates.cend(), it->first);
        }
    }

    Logger::instance().debug("\n");
}

vector<cut> Sketch::find_cuts_with_chain(string ref_l, string ref_r, cut_stats& stats, orientation_en orientation,
                                         unordered_set<read_id_t>& insertion_candidates, bool long_insertion) {
    vector<minimizer_t> left_frw_minimizers, left_rc_minimizers, right_frw_minimizers, right_rc_minimizers;
    vector<hit> left_frw_candidates, left_rc_candidates, right_frw_candidates, right_rc_candidates;
    get_genome_hits(ref_l, left_frw_minimizers, left_frw_candidates);
    get_genome_hits(ref_r, right_frw_minimizers, right_frw_candidates);
    Logger::instance().debug("Minimizers Size: %d\n", left_frw_minimizers.size());
    Logger::instance().debug("Hits Size: %d\n", left_frw_candidates.size());

    vector<cut> left_frw_cuts, right_frw_cuts;

    Logger::instance().debug("--- LEFT ---\n");
    find_left_cuts(left_frw_candidates, left_frw_cuts, ref_l.size(), orientation, left_frw_minimizers.size(), insertion_candidates, left_frw_minimizers, INSERTION_MINIMIZER_CUTOFF);
    Logger::instance().debug("--- RIGHT ---\n");
    find_right_cuts(right_frw_candidates, right_frw_cuts, ref_r.size(), orientation, right_frw_minimizers.size(), insertion_candidates, right_frw_minimizers, INSERTION_MINIMIZER_CUTOFF);

    vector<cut> final_cuts;
    merge_candidates(left_frw_cuts, right_frw_cuts, final_cuts, stats, long_insertion);
    Logger::instance().debug("%d\n", final_cuts.size());

    sort(final_cuts.begin(), final_cuts.end());

#ifdef DEBUG
    for (int  i = 0; i < final_cuts.size(); i++) {
        Logger::instance().debug("%-30s: %4d-%4d (%4d) | ", sequences[final_cuts[i].seq_id].first.c_str(), final_cuts[i].range.start,
                                 final_cuts[i].range.end, final_cuts[i].range.end - final_cuts[i].range.start);
        Logger::instance().debug("I: %d | ", final_cuts[i].estimated_insertion);
        if (final_cuts[i].type == BIMODAL)
            Logger::instance().debug("T: B\n");
        else if (final_cuts[i].type == PARTIAL_LEFT)
            Logger::instance().debug("T: L\n");
        else if (final_cuts[i].type == PARTIAL_RIGHT)
            Logger::instance().debug("T: R\n");
        else if (final_cuts[i].type == OVERLAPPING_READ)
            Logger::instance().debug("T: O\n");
        else
            Logger::instance().debug("T: M\n");
    }
#endif

    return final_cuts;
}

void inline readjust(cut& read_cut_info, int insertion_estimation, offset_t left_extension, offset_t right_extension, bool
reorient) {
    if (read_cut_info.type == BIMODAL && insertion_estimation != -1) {
        if (read_cut_info.estimated_insertion > (1.5 * insertion_estimation))
            read_cut_info.type = DISCARDED;
    }
    else if (read_cut_info.type == PARTIAL_LEFT && left_extension != -1) {
        read_cut_info.range.end = min(read_cut_info.range.end, read_cut_info.range.start + left_extension);
    }
    else if (read_cut_info.type == PARTIAL_RIGHT && right_extension != -1) {
        read_cut_info.range.start = max(read_cut_info.range.start, read_cut_info.range.end - right_extension);
    }
    if (reorient) {
        if (read_cut_info.type == PARTIAL_LEFT)
            read_cut_info.type = PARTIAL_RIGHT;
        else if (read_cut_info.type == PARTIAL_RIGHT)
            read_cut_info.type = PARTIAL_LEFT;
    }
}

pair<vector<cut>, int> Sketch::find_cuts_all(string& ref_l, string& ref_r, bool long_insertion, vector<string>& short_reads) {
    check_insertion_content = !long_insertion;

    vector<cut> cuts, cuts_2;

    unordered_set<read_id_t> insertion_candidates_frw, insertion_candidates_rc;
    if (!short_reads.empty()) {
        int read_len = short_reads[0].size();
        int left_cut_size = min(read_len, (int)ref_l.size());
        int right_cut_size = min(read_len, (int)ref_r.size());

        string genome_left = ref_l.substr(ref_l.size() - left_cut_size, left_cut_size);
        string genome_left_rc = reverse_complement(genome_left);

        string genome_right = ref_r.substr(0, right_cut_size);
        string genome_right_rc = reverse_complement(genome_right);

        vector<string> short_reads_rc;
        short_reads_rc.reserve(short_reads.size());
        for (int i = 0; i < short_reads.size(); i++) {
            short_reads_rc.push_back(reverse_complement(short_reads[i]));
        }

        get_insertion_minimizers(short_reads, genome_left, genome_right, insertion_candidates_frw);
        get_insertion_minimizers(short_reads_rc, genome_left_rc, genome_right_rc, insertion_candidates_rc);
    }

    cut_stats frw_stats, rc_stats;
    Logger::instance().debug("--- FRW ---\n");
    cuts = find_cuts_with_chain(ref_l, ref_r, frw_stats, FRW, insertion_candidates_frw, long_insertion);
    Logger::instance().debug("--- REV ---\n");
    cuts_2 = find_cuts_with_chain(reverse_complement(ref_r), reverse_complement(ref_l), rc_stats, REV, insertion_candidates_rc, long_insertion);

    int insertion_estimation = -1;
    offset_t left_extension = -1, right_extension = -1;
    if (frw_stats.bimodal_cnt + rc_stats.bimodal_cnt > 2) {
        insertion_estimation = (frw_stats.bimodal_sum + rc_stats.bimodal_sum) / (frw_stats.bimodal_cnt + rc_stats.bimodal_cnt);
        left_extension = insertion_estimation + GENOME_ANCHOR_LEN;
        right_extension = insertion_estimation + GENOME_ANCHOR_LEN;
        Logger::instance().debug("IE: %d | LE: %d | RE: %d\n", insertion_estimation, left_extension, right_extension);
    }
    else {
        vector<offset_t> left_sizes(frw_stats.left_cuts_size.size() + rc_stats.right_cuts_size.size());
        vector<offset_t> right_sizes(frw_stats.right_cuts_size.size() + rc_stats.left_cuts_size.size());

        std::merge(frw_stats.left_cuts_size.begin(), frw_stats.left_cuts_size.end(), rc_stats.right_cuts_size.begin(),
                   rc_stats.right_cuts_size.end(), left_sizes.begin());
        Logger::instance().debug("Left: \n");
        for (int i = 0; i < left_sizes.size(); i++) {
            Logger::instance().debug("%d, ", left_sizes[i]);
        }
        Logger::instance().debug("LF: \n");
        for (int i = 0; i < frw_stats.left_cuts_size.size(); i++) {
            Logger::instance().debug("%d, ", frw_stats.left_cuts_size[i]);
        }
        Logger::instance().debug("RR: \n");
        for (int i = 0; i < rc_stats.right_cuts_size.size(); i++) {
            Logger::instance().debug("%d, ", rc_stats.right_cuts_size[i]);
        }
        Logger::instance().debug("\n");
        std::merge(frw_stats.right_cuts_size.begin(), frw_stats.right_cuts_size.end(), rc_stats.left_cuts_size.begin(),
                   rc_stats.left_cuts_size.end(), right_sizes.begin());
        Logger::instance().debug("Right: \n");
        for (int i = 0; i < right_sizes.size(); i++) {
            Logger::instance().debug("%d, ", right_sizes[i]);
        }
        Logger::instance().debug("\n");

        if (left_sizes.size() > 7) {
            left_extension = left_sizes[left_sizes.size() - 4];
        }
        else if (left_sizes.size() > 2) {
            left_extension = left_sizes[left_sizes.size()/2];
        }
        if (right_sizes.size() > 7) {
            right_extension = right_sizes[right_sizes.size() - 4];
        }
        else if (right_sizes.size() > 2) {
            right_extension = right_sizes[right_sizes.size()/2];
        }
        Logger::instance().debug("LE: %d | RE: %d\n", left_extension, right_extension);
    }

    for (int i = 0; i < cuts.size(); i++) {
        readjust(cuts[i], insertion_estimation, left_extension, right_extension, false);
    }
    for (int i = 0; i < cuts_2.size(); i++) {
        readjust(cuts_2[i], insertion_estimation, right_extension, left_extension, true);
    }

    cuts.insert(cuts.end(), cuts_2.begin(), cuts_2.end());

    sort(cuts.begin(), cuts.end());

#ifdef DEBUG
    for (int  i = 0; i < cuts.size(); i++) {
        Logger::instance().debug("%-30s: %4d-%4d (%4d) | ", sequences[cuts[i].seq_id].first.c_str(), cuts[i].range.start,
                                 cuts[i].range.end, cuts[i].range.end - cuts[i].range.start);
        Logger::instance().debug("I: %d | ", cuts[i].estimated_insertion);
        Logger::instance().debug("S: %d | ", cuts[i].number_of_kmers);

        if (cuts[i].type == BIMODAL)
            Logger::instance().debug("T: B\n");
        else if (cuts[i].type == PARTIAL_LEFT)
            Logger::instance().debug("T: L\n");
        else if (cuts[i].type == PARTIAL_RIGHT)
            Logger::instance().debug("T: R\n");
        else if (cuts[i].type == OVERLAPPING_READ)
            Logger::instance().debug("T: O\n");
        else
            Logger::instance().debug("T: M\n");
    }
#endif

    if (frw_stats.overlapping_cnt + rc_stats.overlapping_cnt > 2) {
        return {cuts, -1};
    }

    return {cuts, insertion_estimation};
}