#include "sketch.h"
#include "cut_ranges.h"
#include "partition.h"
#include "p2_partition.h"
#include "p3_partition.h"
#include "common.h"
#include <mutex>
#include "progressbar.h"

using namespace std;

const int genome_anchor_distance = 650;

struct read_cluster {
    vector <pair<pair < string, string>, pair<int, int>>> reads;
    string chrName;
    int pt_start;
    int pt_end;
    int id;
};

class LRIdentification {
    private:
        Sketch* sketch;
        int thread_count;
        p2_partition* find_pt;
        genome_partition* sr_pt;
        string ref;
        string p2;
        string long_read;
        string range;
        string p3;
        string dp;
        ProgressBar* find_progress;
        int total;
        int processed_cnt = 0;
        int genome_anchor_len = 600;

    public:
        LRIdentification(const string &partition_file, const string &dat_path, const string &range, const string &ref,
                         const string &p3_name, const string &longread, const int thread_count, float tga, float ti,
                         float tck, const string&, const int);
        void identify();
        void thread_identify(int tid, vector<pair<read_id_t, range_s> >& ranges);
        read_cluster read_partition();
        void extract(vector<pair<read_id_t, range_s> >&, map<int, size_t>&);
};