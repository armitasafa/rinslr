#ifndef SKETCH_H
#define SKETCH_H

#include <map>
#include <unordered_map>
#include <zlib.h>
#include <string>
#include <vector>
#include <utility>
#include <cstdint>
#include <fstream>
#include <unordered_set>

#include "chain.h"
#include "aligner.h"
#include "progressbar.h"

const int BUFFSIZE = 2000000000;

const int GENOME_ANCHOR_LEN = 600;
const int GENOME_ANCHOR_CUTOFF = 50;
const int BOUNDARY_DISTANCE_CUTOFF = 100;

typedef int read_id_t;
typedef uint32_t hash_t;
typedef int offset_t;
typedef uint32_t hash_size_t;
typedef uint64_t mem_offset_t;
typedef pair<hash_t, offset_t> minimizer_t;

const hash_t MAX_HASH = UINT32_MAX;
const int hash_bytes = 4;

enum orientation_en : u_int8_t {FRW = 0, REV = 1};
enum type_en : u_int8_t {BIMODAL = 0, PARTIAL_LEFT = 1, PARTIAL_RIGHT = 2, OVERLAPPING_READ = 3, DISCARDED = 4};

struct cut_stats {
    int bimodal_cnt = 0;
    int bimodal_sum = 0;
    int overlapping_cnt = 0;
    vector<offset_t> left_cuts_size;
    vector<offset_t> right_cuts_size;
};

struct Location {
    read_id_t seq_id;
    offset_t offset;

    bool operator <(const Location &y) {
        if (seq_id < y.seq_id)
            return true;
        else if (seq_id == y.seq_id) {
            return offset < y.offset;
        }
        return false;
    }
};

struct range_s {
    offset_t start;
    offset_t end;
};

struct hit {
    read_id_t seq_id;
    hash_t hash_value;
    mem_offset_t offset;
    mem_offset_t genome_offset;

    bool operator <(const hit &y) {
        return seq_id == y.seq_id ? hash_value < y.hash_value : seq_id < y.seq_id;
    }

    bool operator () (const read_id_t &i, const hit &a) const {
        return i < a.seq_id;
    }

    bool operator () (const hit &a, const read_id_t &i) const {
        return a.seq_id < i;
    }

    bool operator() (const hit &a, const hit &b) {
        return a.offset < b.offset;
    }
};

struct cut {
    read_id_t seq_id;
    type_en type = DISCARDED;
    range_s range;
    hash_size_t size;
    range_s genome_range;
    int breakpoint_distance;
    orientation_en orientation;
    int estimated_insertion = -1;
    int number_of_kmers = -1;

    bool operator <(const cut &b) {
        if (type < b.type) {
            return true;
        }
        else if (type == b.type) {
            if (number_of_kmers > b.number_of_kmers) {
                return true;
            }
            else if (number_of_kmers == b.number_of_kmers) {
                if (range.end - range.start > b.range.end - b.range.start) {
                    return true;
                }
                return false;
            }
            else
                return false;
        }
        else
            return false;
    }
};

struct minimizer {
    hash_t hash_value;
    mem_offset_t offset;

    bool operator() (const minimizer &b, const hash_t &i) {
        return (b.hash_value < i);
    }
};

class Sketch {
	private:

        gzFile gz_fin;
        char *zbuffer;
        string lr_path;
        string dp_path;
        string file_name;
        uint64_t freq_th = 0;
        int read_id = 0;
        int kmer_size;
        uint64_t file_size;
		int window_size;
        int32_t buff_pos = 0;
        int32_t buff_size = 0;
		int thread_cnt;
		bool check_insertion_content;
        uint64_t total_entries = 0;
		uint64_t total_hashes = 0;

        float DISCARD_FREQ = 0.01;

        string tmp_file_name;

        vector<minimizer> minimizers;
        vector<Location> ref_minimizers;

        unordered_set<hash_t> discard_hashes;

        void load();
        void compute_freq_th();
        void dump(vector<pair<hash_t, Location> >&);
        void dump(unordered_set<hash_t>&);

        void read_buffer();
        uint32_t read_line(string&);
        pair<mem_offset_t, hash_size_t> find_hit(const hash_t&);

	public:
        float INSERTION_MINIMIZER_CUTOFF = 0.05;
        float TGA = 0.3;
        float TCK = 0.6;

        vector<pair<string, offset_t> > sequences;
        ClaspChain claspChain;

        Sketch();
		Sketch(string, string);
        Sketch(string, string, int t, int k, int w, float df);
        void build_sketch();
        void get_ref_minimizers(char*, read_id_t, int, vector<pair<hash_t, Location> >&);
        void get_ref_minimizers(char* read, read_id_t id, int len, vector<pair<hash_t, Location> > &ref_minimizers_vec,
                       unordered_map<hash_t, hash_size_t> &hashes);
        void get_query_minimizers(char*, read_id_t, offset_t, vector<minimizer_t>&);
        pair<vector<cut>, int> query(vector<string>&, bool, string&, string&);
        pair<vector<cut>, int> find_cuts_all(string&, string&, bool, vector<string>&);
        void get_genome_hits(string&, vector<minimizer_t>&, vector<hit>&);
        vector<hit> get_hits(vector<minimizer_t>&);
        vector<cut> find_cuts_with_chain(string, string, cut_stats&, orientation_en, unordered_set<read_id_t>&, bool);
        vector<seed> create_seeds(vector<hit>&, int, int);
        MaxChainInfo get_genome_anchor(vector<hit>&, int, int);
        void find_left_cuts(vector<hit>&, vector<cut>&, int, orientation_en, int, unordered_set<read_id_t>&,
                vector<minimizer_t>&, float);
        void find_right_cuts(vector<hit>&, vector<cut>&, int, orientation_en, int, unordered_set<read_id_t>&,
                vector<minimizer_t>&, float);
        void merge_candidates(vector<cut>&, vector<cut>&, vector<cut>&, cut_stats&, bool);
        void get_insertion_minimizers(vector<string>&, string&, string&, unordered_set<read_id_t>&);
        void get_unique_minimizers(vector<string>&, unordered_set<hash_t>&);
        inline int max_kmer_count(MaxChainInfo, int, int, int, vector<minimizer_t>&);

        void discard_high_freq(vector<pair<hash_t, Location> > &ref_minimizers_vec, unordered_set<hash_t>& high_freq,
                               uint64_t& total_entries);
        void merge_sizes(unordered_map<hash_t, hash_size_t> &a, unordered_map<hash_t, hash_size_t> &b);

        void merge(string, string, string);
        void cleanup_vectors(vector<pair<hash_t, Location> >& avec, vector<pair<hash_t, Location> >& bvec, int& arem, int& brem,
                                 ofstream& fout);
        void merge_vectors(vector<pair<hash_t, Location> >& avec, vector<pair<hash_t, Location> >& bvec, int& arem, int& brem,
                               bool, bool, ofstream& fout);
        void fill_vector(vector<pair<hash_t, Location> >& v, uint64_t v_size, uint64_t& vptr, int& vrem, ifstream& vfin,
                             bool& done);
        void build_sketch_mt(int id, const ProgressBar progress, unordered_map<hash_t, hash_size_t> &high_freq_hashes);
        void merge_thread_files(int thread_id, int file_id, int& file_no);
		void dump_vector(vector<pair<hash_t, Location> >&, unordered_set<hash_t>&, vector<minimizer>&, hash_t&, uint64_t&,
						uint64_t&, ofstream&);
		void merge_and_dump(string, string, unordered_set<hash_t>&);
		void merge_and_dump_vectors(vector<pair<hash_t, Location> >& avec, vector<pair<hash_t, Location> >& bvec, int& arem, int& brem,
							bool adone, bool bdone, ofstream& fout, unordered_set<hash_t>& discard_set, vector<minimizer>& hash_sizes, 
							hash_t& prv_hash, uint64_t& hash_entries, uint64_t& cnt);
        void dump_discard_set();
        void load_discard_set();
};

#endif
