#ifndef __PROCESS_PARTITION__
#define __PROCESS_PARTITION__

#include <string>

#include "sketch.h"
#include "cut_ranges.h"
#include "progressbar.h"
#include "p3_partition.h"
#include "insertion_assembler.h"

const int BIMODAL_THRESHOLD = 300;

struct cluster {
    string chrName;
    int cluster_id;
    int pt_start;
    int pt_end;
    pair<vector<read_cut_info >, classified_cuts> reads;
    int estimated_insertion;
    type_en cluster_type;
};

class ProcessPartition {
    private:
        int total;
        int max_len = 30000;
        FILE *fo_vcf;
        FILE *fo_log;
        Sketch* sketch;
        FILE *fo_vcf_lq;
        int long_no = 0;
        int single_no = 0;
        int bimodal_no = 0;
        int LENFLAG	= 1000;
        int max_threads = 1;
        string reference_name;
        ProgressBar* progress;
        int processed_cnt = 0;
        cut_ranges* extractor;
        InsertionAssembler* ia;
        p3_partition* partition;
        int MAX_REF_LEN	= 300000000;
        int MAX_SEG_LEN = -1;

    public:
        ProcessPartition(int maxThreads, const string &lrPath, const string &dat_path,
                         const string &partition, const string &range,
                         const string &reference, const string &prefix, const int max_len);
        cluster get_cluster();
        void thread_process(int tid);
        void process();
};


#endif
