/*
 * clasp is written by Christian Otto, Bioinformatics, University of Leipzig
 * https://www.bioinf.uni-leipzig.de/Software/clasp/
 *
 * This is a wrapper class designed for Pamir.
 */

#ifndef CHAIN_H
#define CHAIN_H

#include <vector>

using namespace std;

#define SOP     ((unsigned char) (0 << 0))
#define LIN     ((unsigned char) (1 << 0))

struct MaxChainInfo {
    pair<int, int> qrange;
    pair<int, int> rrange;
    float score;
    int len = 0;
    int gaps_size;
};

struct seed {
    int qPos;
    int sPos;
    int len;
};

class ClaspChain {
    private:
        char chainmode;
        double lambda;
        double epsilon;
        int maxgap;

    public:
        ClaspChain(char chainmode = SOP, double lambda = 1.0, double epsilon = 0, int maxgap = -1);
        MaxChainInfo get_max_chain(vector<seed>& seeds);
        double sort_time = 0;
        double chain_time = 0;
};

#endif
