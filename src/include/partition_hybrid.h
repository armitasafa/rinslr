#ifndef __PARTITION_HYBRID__
#define __PARTITION_HYBRID__

#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
#include <utility>
#include <zlib.h>
#include <unordered_set>

#include "sam_processing.h"

using namespace std;

const int MIN_Q = 30;
const int WINDOW_SIZE = 20000;
const int MARGIN = 5;

const int GENOME_OFFSET_INIT = 5;
const int ARRAY_OFFSET_INIT = 5;

const int CLUSTER_SIZE = 150;

const int MERGE_RANGE = 500;

struct BreakpointCandidate {
    int support = 0;
    vector<pair<string, string> > reads;
    string chr;
    int window_support = 0;
    int nbr_stretch = 1;
};

class genome_partition_hybrid {
private:
    FILE* partition_out_file = NULL;
    FILE* partition_out_index_file = NULL;
    FILE* partition_out_count_file = NULL;

    int partition_count = 0;
    int partition_id = 1;

    int p_start;
    int p_end;
    string p_ref;

    //TODO: more todo
    int start;
    int end;
    FILE* partition_file = NULL;

    int total;

    vector<pair<pair<string, string>, pair<int,int> > > current_cluster;

    string line;
    string curr_chr;

    pair<int, int> potential_cluster = {-1, -1};
    vector<pair<string, string> > potential_reads;
    bool found_merged_cluster = false;

    int MIN_SUPPORT = 10;

private:
	void add_read(string, int, int);

public:
	genome_partition_hybrid(const string&, const string&, bool write_to_file = false);
	genome_partition_hybrid(int, const string&,  const string&, const string&, const string&, const string &);
	~genome_partition_hybrid(void);
	genome_partition_hybrid(const string&, bool, int support = 10);

    void cluster_reads(string map_path);

	vector<pair<pair<string, string>, pair<int,int> > > read_partition();
	
	int get_start(void);
	int get_end(void);
	int get_id();
    int clean_up(vector<BreakpointCandidate>& locs, int offset, bool, int);
    void dump_cluster(vector<pair<string, string> > reads, int, int);
    pair<string, int> inline fill(vector<BreakpointCandidate>& locs, int genome_offset, ifstream& fin);
    void try_to_merge(int, int, vector<pair<string, string> >);
};


#endif
