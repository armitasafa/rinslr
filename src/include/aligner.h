#ifndef __ALIGNER__
#define __ALIGNER__

#include<string>

using namespace std;

const int INS_MIN_LEN = 20;
const int MIN_ANCHOR = 50;

enum event_en : u_int8_t {INS = 0, DEL = 1};

struct event {
    event_en type;
    int left_flank = 0;
    int right_flank = 0;
    int cluster_total = 0;
    int len = 0;
    string content;
    int start;
    int end;
};

class aligner {
private:
    int REF_LEN = 2500;
    int CON_LEN = 10000;
    int MAX_SIDE_A = 15000;
	int MAX_SIDE_B = 15000;
	int MAX_SIDE = 15000;

//    static const int MATCH_SCORE = 20;
//    static const int MISMATCH_SCORE = -250;
//    static const int GAP_OPENING_SCORE = -500;
//    static const int GAP_EXTENSION_SCORE = -1;

//    static const int MATCH_SCORE = 20;
//    static const int MISMATCH_SCORE = -500;
//    static const int GAP_OPENING_SCORE = -1000;
//    static const int GAP_EXTENSION_SCORE = -1;

    static const int MATCH_SCORE = 5;
    static const int MISMATCH_SCORE = -4;
    static const int GAP_OPENING_SCORE = -10;
    static const int GAP_EXTENSION_SCORE = -1;
/*
    static const int MATCH_SCORE = 20;
	static const int MISMATCH_SCORE = -20;
	static const int GAP_OPENING_SCORE = -100;
	static const int GAP_EXTENSION_SCORE = 0;

    static const int MATCH_SCORE = 1000;
	static const int MISMATCH_SCORE = -1000;
	static const int GAP_OPENING_SCORE = -1000;
	static const int GAP_EXTENSION_SCORE = -1;
*/
    string a;
    string b;
    string c;
    int **score;
    int **gapa;
    int **gapb;
    double identity;
    int p_start;
    int p_end;
    int a_start;
    int a_end;
    int anchor_len;
    int left_anchor;
    int right_anchor;
    int ref_abs_start;

    bool clipped = false;
private:
    void print_matrix(string, const string &, const string &, int **);
    void clear(int, int);

public:
    aligner(int reflen = 3000, int qlen = 10000);

    ~aligner();

    void align(const string &, const string &);

    int extract_calls(const int &, vector <tuple<string, int, int, string, int, float>> &,
                      vector <tuple<string, int, int, string, int, float>> &, const int &, const int &, string);

    int extract_calls(const int &, vector <tuple<string, int, int, string, int, float>> &,
    vector <tuple<string, int, int, string, int, float>> &, const int &, const int &, string, string &);

    int extract_calls(const int &, vector <tuple<string, int, int, string, int, float>> &,
                      vector <tuple<string, int, int, string, int, float, int, int, int>> &, const int &, const int &, string, string &);

    void dump(string);//void dump(FILE *fo, string);
    void dump(string, string&);
    int get_start();

    int get_end();

    float get_identity();

    int get_left_anchor();

    int get_right_anchor();

    string get_a();

    string get_b();

    void empty();
};

#endif

