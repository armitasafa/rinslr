#ifndef __COMMANDPARSER_H__
#define __COMMANDPARSER_H__

#include <string>

#include "common.h"

using namespace std;

enum rinslr_mode : int {CLUSTER = 0, INDEX = 1, FIND = 2, CALL = 3, MISC = 4};

class CommandParser {
public:
    string short_reads_file = "";
    int read_len = 0;
    int kmer_size = 15;
    int window_size = 10;
    string out_dir_prefix = ".";
    string long_reads_file = "";
    string partition_path = "";
    string range = "";
    string reference_file = "";
    string output_prefix;
    int silent = 0;
    string log_file_path = "";
    int threads = 1;
    string dat_path = "";
    int anchor_len = 600;
    float discard_freq = 0.01;

    int td = 10;

    int max_len = -1;

    float tga = 0.3;
    float ti = 0.05;
    float tck = 0.6;

    rinslr_mode mode = MISC;

    int parse(int argc, char *argv[]);
    int parse_index_command(int argc, char *argv[]);
    int parse_cluster_command(int argc, char *argv[]);
    int parse_find_command(int argc, char *argv[]);
    int parse_call_command(int argc, char *argv[]);

    void index_help();
    void find_help();
    void cluster_help();
    void call_help();
    void version();
    void help();
};

#endif