#include "LRIdentification.h"

#include "genome.h"

#include <thread>
#include <algorithm>

mutex read_cluster_mutex;
mutex write_mutex;

LRIdentification::LRIdentification(const string &partition_file, const string &dat_path,
                                   const string &r, const string &reference, const string &p3_name,
                                   const string &longread, const int threads, float tga, float ti, float tck, const
                                   string& curr_dir, const int anchor_len) {
    sketch = new Sketch(longread, dat_path);
    sketch->TGA = tga;
    sketch->TCK = tck;
    sketch->INSERTION_MINIMIZER_CUTOFF = ti;
    genome_anchor_len = anchor_len;

    sr_pt = new genome_partition(partition_file, r);

    //TODO: fix path
    range = sr_pt->get_range();
    p2 = curr_dir + "/cluster-temp-" + range;
    find_pt = new p2_partition(p2, true);

    ref = reference;
    long_read = longread;
    p3 = p3_name;
    thread_count = threads;
    dp = dat_path;

    total = sr_pt->get_total();

    find_progress = new ProgressBar(80);
    char comment[COMMENT_LENGTH];
    snprintf(comment, COMMENT_LENGTH, "%10d / %-10d", 0, total);
    find_progress->update((0.0/(float)total) * 100, comment);
}

read_cluster LRIdentification::read_partition() {
    read_cluster_mutex.lock();

    auto p = sr_pt->read_partition();

    int start = sr_pt->get_start();
    int end = sr_pt->get_end();
    int id = sr_pt->get_id();

    read_cluster new_cluster;
    new_cluster.reads = p;
    new_cluster.pt_start = start;
    new_cluster.pt_end = end;
    new_cluster.id = id;
    new_cluster.chrName = sr_pt->get_reference();

    if (p.size() != 0) {
        char comment[COMMENT_LENGTH];
        processed_cnt += 1;
        snprintf(comment, COMMENT_LENGTH, "%10d / %-10d", processed_cnt, total);
        find_progress->update(((float)processed_cnt/(float)total) * 100, comment);
        Logger::instance().debug("ID: %d\n", processed_cnt);
    }

    read_cluster_mutex.unlock();

    return new_cluster;
}

void LRIdentification::thread_identify(int tid, vector<pair<read_id_t, range_s> >& ranges) {
    ranges.reserve(100000);

    genome reference(ref.c_str());

    while (1)
    {

        auto p 			= read_partition();

        // end of the partition file
        if (!p.reads.size())
            break;

        pair<vector<cut>, int> cut_results;
        vector<cut> cut_candidates;

        string chrName  = p.chrName;
        int pt_start    = p.pt_start;
        int pt_end      = p.pt_end;
        int id          = p.id;

        // cluster has too many or too few reads
        if ( p.reads.size() > 7000 || p.reads.size() <= 2 ) {
            write_mutex.lock();
            find_pt->add_cuts(p.reads, cut_candidates, pt_start, pt_end, chrName, -1, id);
            write_mutex.unlock();
            continue;
        }
        vector<string> reads;
        reads.reserve(p.reads.size());
        for (int i = 0; i < p.reads.size(); i++) {
            reads.push_back(p.reads[i].first.second);
        }

        int breakpoint = (pt_end + pt_start)/2;
        int left_start = breakpoint - genome_anchor_len;
        int left_end = breakpoint;
        int right_start = breakpoint;
        int right_end = breakpoint + genome_anchor_len;

        string ref_l = reference.get_bases_at(chrName, left_start, left_end);
        string ref_r = reference.get_bases_at(chrName, right_start, right_end);

        cut_results = sketch->query(reads, false, ref_l, ref_r);
        cut_candidates = cut_results.first;

        int estimated_insertion = cut_results.second;

        for (int i = 0; i < cut_candidates.size(); i++) {
            ranges.push_back({cut_candidates[i].seq_id, cut_candidates[i].range});
        }

        if (cut_candidates.size() == 0) {
            write_mutex.lock();
            find_pt->add_cuts(p.reads, cut_candidates, pt_start, pt_end, chrName, -1, id);
            write_mutex.unlock();
            continue;
        }

        write_mutex.lock();
        find_pt->add_cuts(p.reads, cut_candidates, pt_start, pt_end, chrName, estimated_insertion, id);
        write_mutex.unlock();
    }
}

void LRIdentification::extract(vector<pair<read_id_t, range_s>>& ranges, map<int, size_t>& offsets) {
   string new_range = to_string(1) + "-" + to_string(processed_cnt);
    p2_partition extract_read_pt(p2, range, offsets);
    p3_partition extract_write_pt(p3, true, extract_read_pt.get_range_start());
    ProgressBar extract_progress(80);
    int cnt = 0;
    int total = extract_read_pt.get_total();
    char comment[COMMENT_LENGTH];
    snprintf(comment, COMMENT_LENGTH, "%10d / %-10d", cnt, total);
    extract_progress.update(((float)cnt/(float)total) * 100, comment);
    string sq = dp + "/" + get_file_name(long_read) + ".sq.idx";
    cut_ranges extractor = cut_ranges(long_read, sq, ranges);
    extractor.extract();

    while (1) {
        cnt++;

        auto p 			= extract_read_pt.read_partition();

        if ( !p.first.size() )
            break;

        snprintf(comment, COMMENT_LENGTH, "%10d / %-10d", cnt, total);
        extract_progress.update(((float)cnt/(float)total) * 100, comment);

        vector<p3_read_s> cuts;

        string chrName  = extract_read_pt.get_reference();
        int pt_start    = extract_read_pt.get_start();
        int pt_end      = extract_read_pt.get_end();

        // cluster has too many or too few reads
        if ( p.first.size() > 7000 || p.first.size() <= 2 ) {
            Logger::instance().info("-<=*=>-*-<=*=>-*-<=*=>-*-<=*=>-*-<=*=>-*-<=*=>-*-<=*=>-*-<=*=>-*-<=*=>-*-<=*=>-\n");
            Logger::instance().info(" + Cluster ID      : %d\n", extract_read_pt.get_id());
            Logger::instance().info(" + Short Reads Count     : %lu\n", p.first.size());
            Logger::instance().info("INFO: Skipped Processing - Too few or Too many short reads\n");

            extract_write_pt.add_reads(p.first, cuts, pt_start, pt_end, chrName, extract_read_pt
            .get_estimated_insertion(), extract_read_pt.get_id());
            continue;
        }
            // cluster has too many or too few reads
        else if ( p.second.size() == 0 ) {
            Logger::instance().info("-<=*=>-*-<=*=>-*-<=*=>-*-<=*=>-*-<=*=>-*-<=*=>-*-<=*=>-*-<=*=>-*-<=*=>-*-<=*=>-\n");
            Logger::instance().info(" + Cluster ID      : %d\n", extract_read_pt.get_id());
            Logger::instance().info(" + Short Reads Count     : %lu\n", p.first.size());
            Logger::instance().info("INFO: Skipped Processing - No long reads found\n");

            extract_write_pt.add_reads(p.first, cuts, pt_start, pt_end, chrName, extract_read_pt.get_estimated_insertion(), extract_read_pt.get_id());
            continue;
        }

        for (int i = 0; i < p.second.size(); i++) {
            pair<string, string> tmp = extractor.get_cut(p.second[i].id, p.second[i].range.start,
                                                         p.second[i].range.end);
            string cut;
            if (p.second[i].orientation == REV) {
                cut = reverse_complement(tmp.second);
            }
            else
                cut = tmp.second;
            cuts.push_back((p3_read_s) {.name = tmp.first, .sequence = cut, .range = p.second[i].range,
                    .type = p.second[i].type, .orientation = p.second[i].orientation});
        }

        extract_write_pt.add_reads(p.first, cuts, pt_start, pt_end, chrName, extract_read_pt.get_estimated_insertion(), extract_read_pt.get_id());
    }
}

void LRIdentification::identify() {
    vector<pair<read_id_t, range_s> > ranges[thread_count];

    thread threads[thread_count];
    for (int i = 0; i < thread_count; i++)
        threads[i] = thread(&LRIdentification::thread_identify, this, i, std::ref(ranges[i]));
    for (int i = 0; i < thread_count; i++)
        threads[i].join();

    vector<pair<read_id_t, range_s> > merged_ranges;
    merged_ranges.reserve(10000000);
    for (int i = 0; i < thread_count; i++) {
        merged_ranges.insert(merged_ranges.end(), ranges[i].begin(), ranges[i].end());
    }

    sort(merged_ranges.begin(), merged_ranges.end(),
         [](const pair<read_id_t, range_s> &a, const pair<read_id_t, range_s> &b) {
                    return a.first < b.first;
    });
    uint32_t size = merged_ranges.size();
    uint32_t negs = 0;

    if (!merged_ranges.empty()) {
        for (uint32_t i = 1; i < merged_ranges.size(); i++) {
            if (merged_ranges[i].first != merged_ranges[i-1].first)
                continue;
            else {
                merged_ranges[i].second.start = min(merged_ranges[i].second.start, merged_ranges[i-1].second.start);
                merged_ranges[i].second.end = max(merged_ranges[i].second.end, merged_ranges[i-1].second.end);
                merged_ranges[i-1].first = -1;
                negs++;
            }
        }
    }

    sort(merged_ranges.begin(), merged_ranges.end(), [](const pair<read_id_t, range_s> &a, const pair<read_id_t, range_s> &b) {
        return a.first < b.first;
    });

    map<int, size_t> offsets = find_pt->get_cluster_offsets();

    find_pt->close();

    extract(merged_ranges, offsets);
    
    unlink((p2 + ".count").c_str());
}
