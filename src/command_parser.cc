#include "command_parser.h"

#include <string>
#include <iostream>
#include <getopt.h>

using namespace std;

const int MAX_FILE_NAME_LEN = 1000;

inline void error(string msg) {
    cout << "Error: " << msg << endl;
    cout << "Use --help to see available options and usages.\n\n";
}

int CommandParser::parse(int argc, char* argv[]) {
    if (argc < 2) {
        cout << "Invalid mode.\n\n";
        help();
        return 1;
    }
    string cmd = argv[1];
    if (cmd == "cluster") {
        mode = CLUSTER;
        return parse_cluster_command(argc, argv);
    }
    else if (cmd == "index") {
        mode = INDEX;
        return parse_index_command(argc, argv);
    }
    else if (cmd == "find") {
        mode = FIND;
        return parse_find_command(argc, argv);
    }
    else if (cmd == "call") {
        mode = CALL;
        return parse_call_command(argc, argv);
    }
    else if (cmd == "--help" || cmd == "-h") {
        help();
        return 1;
    }
    else if (cmd == "--version" || cmd == "-v") {
        version();
        return 1;
    }
    else {
        cout << "Invalid mode.\n\n";
        help();
        return 1;
    }
}

int CommandParser::parse_index_command(int argc, char *argv[]) {
    static struct option long_opt[] =
            {
                    {"long-reads",  required_argument, 0,       'l'},
                    {"kmer-size",   required_argument, 0,       'k'},
                    {"window-size", required_argument, 0,       'w'},
                    {"output-dir",  required_argument, 0,       'd'},
                    {"log",         required_argument, 0,       'g'},
                    {"help",        no_argument,       0,       'h'},
                    {"threads",        no_argument,       0,       't'},
                    {"freq",        no_argument,       0,       'f'},
                    {0, 0,                             0,       0},
            };

    int opt;
    int opt_index;

    string log = "";

    while (-1 !=
           (opt = getopt_long(argc, argv, ":l:k:w:d:t:g:hf:", long_opt, &opt_index))) {
        switch (opt) {
            case 'l': {
                long_reads_file = string(optarg);
                break;
            }
            case 'd': {
                out_dir_prefix = string(optarg);
                break;
            }
            case 'k': {
                kmer_size = atoi(optarg);
                break;
            }
            case 'w': {
                window_size = atoi(optarg);
                break;
            }
            case 'g': {
                log = string(optarg);
                break;
            }
            case ':': {
                string msg = "Missing value for -";
                msg.push_back((char)optopt);
                error(msg);
                exit(1);
            }
            case '?': {
                string msg = "Unknown option for -";
                msg.push_back((char)optopt);
                error(msg);
                exit(1);
            }
            case 'h': {
                index_help();
                exit(0);
            }
            case 't': {
                threads = atoi(optarg);
                break;
            }
            case 'f': {
                discard_freq = stof(optarg);
                break;
            }
        }
    }

//    if (silent && log_file_path != "") {
//        error("log file cannot be set in silent mode.");
//        exit(1);
//    }

    if (long_reads_file == "") {
        error("long reads file is not set.");
        exit(1);
    }

    if (log != "")
        log_file_path = out_dir_prefix + "/" + log;

    return 0;
}

int CommandParser::parse_cluster_command(int argc, char *argv[]) {
    static struct option long_opt[] =
            {
                    {"short-reads",              required_argument,       0, 's'},
                    {"output-dir",              required_argument,       0, 'd'},
                    {"log",         required_argument, 0,       'g'},
                    {"help",        no_argument,       0,       'h'},
                    {"min-support",              no_argument,       0, 'w'},
                    {0,                   0,                 0, 0},
            };

    int opt;
    int opt_index;

    string log = "";

    while (-1 !=
           (opt = getopt_long(argc, argv, "s:w:d:g:h", long_opt, &opt_index))) {
        switch (opt) {
            case 's': {
                short_reads_file = string(optarg);
                break;
            }
            case 'w': {
                short_reads_file = string(optarg);
                td = stoi(optarg);
                break;
            }
            case 'd': {
                out_dir_prefix = string(optarg);
                break;
            }
            case 'g': {
                log = string(optarg);
                break;
            }
            case ':': {
                string msg = "Missing value for -";
                msg.push_back((char)optopt);
                error(msg);
                exit(1);
            }
            case '?': {
                string msg = "Unknown option for -";
                msg.push_back((char)optopt);
                error(msg);
                exit(1);
            }
            case 'h': {
                cluster_help();
                exit(0);
            }
        }
    }

//    if (silent && log_file_path != "") {
//        error("log file cannot be set in silent mode.");
//        exit(1);
//    }

    if (short_reads_file == "") {
        error("short reads file is not set.");
        exit(1);
    }

    if (log != "")
        log_file_path = out_dir_prefix + "/" + log;

    return 0;
}

int CommandParser::parse_find_command(int argc, char *argv[]) {
    static struct option long_opt[] =
            {
                    {"long-read",              required_argument,       0, 'l'},
                    {"output-dir",              required_argument,       0, 'd'},
                    {"cluster-file",              required_argument,       0, 'c'},
                    {"reference",              required_argument,       0, 'r'},
                    {"output-prefix",              required_argument,       0, 'o'},
                    {"range",              required_argument,       0, 'R'},
                    {"index",              required_argument,       0, 'i'},
                    {"log",         required_argument, 0,       'g'},
                    {"help",        no_argument,       0,       'h'},
                    {"threads",        no_argument,       0,       't'},
                    {"genome-anchor",        no_argument,       0,       'x'},
                    {"insertion-content",        no_argument,       0,       'z'},
                    {"chain",        no_argument,       0,       'b'},
                    {"anchor",        no_argument,       0,       'a'},
                    {"preset",        no_argument,       0,       'p'},
                    {0,                   0,                 0, 0},
            };

    int opt;
    int opt_index;

    string out;
    string log = "";

    while (-1 !=
           (opt = getopt_long(argc, argv, "l:d:c:r:i:o:t:R:g:hx:z:b:a:p:", long_opt, &opt_index))) {
        switch (opt) {
            case 'l': {
                long_reads_file = string(optarg);
                break;
            }
            case 'd': {
                out_dir_prefix = string(optarg);
                break;
            }
            case 'c': {
                partition_path = string(optarg);
                break;
            }
            case 'r': {
                reference_file = string(optarg);
                break;
            }
            case 'R': {
                range = string(optarg);
                break;
            }
            case 'o': {
                out = string(optarg);
                break;
            }
            case 'i': {
                dat_path = string(optarg);
                break;
            }
            case 'g': {
                log = string(optarg);
                break;
            }
            case ':': {
                string msg = "Missing value for -";
                msg.push_back((char)optopt);
                error(msg);
                exit(1);
            }
            case '?': {
                string msg = "Unknown option for -";
                msg.push_back((char)optopt);
                error(msg);
                exit(1);
            }
            case 'h': {
                find_help();
                exit(0);
            }
            case 't': {
                threads = atoi(optarg);
                break;
            }
            case 'x': {
                tga = stof(optarg);
                break;
            }
            case 'z': {
                ti = stof(optarg);
                break;
            }
            case 'b': {
                tck = stof(optarg);
                break;
            }
            case 'a': {
                anchor_len = atoi(optarg);
                break;
            }
			case 'p': {
                string optargstr = optarg;
				if (optargstr == "pb") {
					tga = 0.3;
					ti = 0.05;
					tck = 0.6;
				}
				else if (optargstr == "ont") {
					tga = 0.2;
					ti = 0;
					tck = 0.2;
				}
				else {
					string msg = "Unknown option for -p";
                	msg.push_back((char)optopt);
                	error(msg);
                	exit(1);
				}
			}
        }
    }

    if (long_reads_file == "" | partition_path == "" | reference_file == "" | out == "") {
        error("Invalid usage. Please provide all required argument.");
        exit(1);
    }

    output_prefix = out_dir_prefix + "/" + out;

    if (log != "")
        log_file_path = out_dir_prefix + "/" + log;

    if (dat_path == "")
        dat_path = get_path(long_reads_file);

    return 0;
}

int CommandParser::parse_call_command(int argc, char *argv[]) {
    static struct option long_opt[] =
            {
                    {"long-read",              required_argument,       0, 'l'},
                    {"output-dir",              required_argument,       0, 'd'},
                    {"output-prefix",              required_argument,       0, 'o'},
                    {"reference",              required_argument,       0, 'r'},
                    {"cluster-file",              required_argument,       0, 'c'},
                    {"range",              required_argument,       0, 'R'},
                    {"index",              required_argument,       0, 'i'},
                    {"log",         required_argument, 0,       'g'},
                    {"help",        no_argument,       0,       'h'},
                    {"threads",        no_argument,       0,       't'},
                    {"genome-anchor",        no_argument,       0,       'x'},
                    {"insertion-content",        no_argument,       0,       'z'},
                    {"chain",        no_argument,       0,       'b'},
                    {"max-segment",        no_argument,       0,       'm'},
					{"preset",        no_argument,       0,       'p'},
                    {0,                   0,                 0, 0},
            };

    int opt;
    int opt_index;

    threads = 16;

    string out;
    string log = "";

    while (-1 !=
           (opt = getopt_long(argc, argv, "l:d:o:r:c:i:R:m:t:g:hx:z:b:p:m:", long_opt, &opt_index))) {
        switch (opt) {
            case 'l': {
                long_reads_file = string(optarg);
                break;
            }
            case 'd': {
                out_dir_prefix = string(optarg);
                break;
            }
            case 'o': {
                out = string(optarg);
                break;
            }
            case 'c': {
                partition_path = string(optarg);
                break;
            }
            case 'R': {
                range = string(optarg);
                break;
            }
            case 'r': {
                reference_file = string(optarg);
                break;
            }
            case 'i': {
                dat_path = string(optarg);
                break;
            }
            case 'g': {
                log = string(optarg);
                break;
            }
            case ':': {
                string msg = "Missing value for -";
                msg.push_back((char)optopt);
                error(msg);
                exit(1);
            }
            case '?': {
                string msg = "Unknown option for -";
                msg.push_back((char)optopt);
                error(msg);
                exit(1);
            }
            case 'h': {
                call_help();
                exit(0);
            }
            case 't': {
                threads = atoi(optarg);
                break;
            }
            case 'x': {
                tga = stof(optarg);
                break;
            }
            case 'z': {
                ti = stof(optarg);
                break;
            }
            case 'b': {
                tck = stof(optarg);
                break;
            }
            case 'm': {
                max_len = stoi(optarg);
                break;
            }
			case 'p': {
                string optargstr = optarg;
				if (optargstr == "pb") {
					tga = 0.3;
					ti = 0.05;
					tck = 0.6;
				}
				else if (optargstr == "ont") {
					tga = 0.2;
					ti = 0;
					tck = 0.2;
				}
				else {
					string msg = "Unknown option for -p";
                	msg.push_back((char)optopt);
                	error(msg);
                	exit(1);
				}
			}
        }
    }

    if (long_reads_file == "" | partition_path == "" | reference_file == "" | out == "") {
        error("Invalid usage. Please provide all required argument.");
        exit(1);
    }

    output_prefix = out_dir_prefix + "/" + out;

    if (log != "")
        log_file_path = out_dir_prefix + "/" + log;

    if (dat_path == "")
        dat_path = get_path(long_reads_file);

    return 0;
}

void CommandParser::index_help() {
    string msg;
    msg = string("\nindex\tBuild an index from the long-reads\n\n") +
          "\t--long-reads, -l    path to long-reads fasta file\n" +
          "\t--kmer-size, -k     length of minimizers (default: " + to_string(kmer_size) + ")\n" +
          "\t--window-size, -w   size of window (default: " + to_string(window_size) + ")\n" +
          "\t--output-dir, -d    path to output directory (default: same as long-reads file)\n" +
          "\t--threads, -t       number of threads (default: " + to_string(threads) + ")\n" +
          "\t--freq, -f          percentage of most frequent minimizers to be discarded (default: " + to_string(discard_freq) + ")\n" +
          "\t--log, -g           write logs to file (default: stdout)\n" +
          "\t--help, -h          list of available options and usages\n";
    cout << msg;
}

void CommandParser::cluster_help() {
    string msg;
    msg = string("\ncluster\tIdentify potential breakpoint locations and create clusters from short-reads\n\n") +
          "\t--short-reads-sam, -s     path to short-reads sam file\n" +
          "\t--min-support, -w         minimum number of reads supporting a breakpoint (default: " + to_string(td) + ") - set according to dateset coverage\n" +
          "\t--output-dir, -d          path to output directory (default: " + out_dir_prefix + ")\n" +
          "\t--log, -g                 write logs to file (default: stdout)\n" +
          "\t--help, -h                list of available options and usages\n";
    cout << msg;
}

void CommandParser::find_help() {
    string msg = string("\nfind\tIdentify long read candidates\n\n") +
                 "\t--long-reads, -l        path to long-reads fasta file\n" +
                 "\t--output-dir, -d        path to output directory (default: " + out_dir_prefix + ")\n" +
                 "\t--cluster-file, -c      path to short reads cluster file\n" +
                 "\t--reference, -r         path to reference genome fasta file\n" +
                 "\t--output-prefix, -o     output file name prefix\n" +
                 "\t--range, -R             range of clusters to be processed (default: all)\n" +
				 "\t--threads, -t           number of threads (default: " + to_string(threads) + ")\n" +
                 "\t--index, -i             path to index files directory (default: same as long-reads file)\n" +
				 "\t--genome-anchor, -x     minimum percentage of anchor minimizers present in a candidate long-read (default: " + to_string(tga) + ")\n" +
				 "\t--insertion-content, -z minimum percentage of insertion minimizers present in a candidate long-read (default: " + to_string(ti) + ")\n" +
				 "\t--chain, -b             minimum percentage of chain minimizers present in a candidate long-read (default: " + to_string(tck) + ")\n" +
				 "\t--preset, -p            technology-specific default values for -x, -z, and -b options (modes: pb (pacbio) and ont (nanopore)\n" +
                 "\t--log, -g               write logs to file (default: stdout)\n" +
                 "\t--help, -h              list of available options and usages\n";
    cout << msg;
}

void CommandParser::call_help() {
    string msg = string("call\tCall insertions events\n\n") +
                 "\t--long-reads, -l        path to long-reads fasta file\n" +
                 "\t--output-dir, -d        path to output directory (default: " + out_dir_prefix + ")\n" +
                 "\t--output-prefix, -o     output file name prefix\n" +
                 "\t--reference, -r         path to reference fasta genome\n" +
                 "\t--cluster-file, -c      path to final cluster file\n" +
                 "\t--range, -R             range of clusters to be processed (default: all)\n" +
                 "\t--index, -i             path to index files directory (default: same as long-reads file)\n" +
                 "\t--threads, -t           number of threads (default: " + to_string(threads) + ")\n" +
				 "\t--genome-anchor, -x     minimum percentage of anchor minimizers present in a candidate long-read (default: " + to_string(tga) + ")\n" +
				 "\t--insertion-content, -z minimum percentage of insertion minimizers present in a candidate long-read (default: " + to_string(ti) + ")\n" +
				 "\t--chain, -b             minimum percentage of chain minimizers present in a candidate long-read (default: " + to_string(tck) + ")\n" +
				 "\t--preset, -p            technology-specific default values for -x, -z, and -b options (modes: pb (pacbio) and ont (nanopore)\n" +
				 "\t--max-segment, -m       maximum length of long-read candidate segments (default: no maximum)\n" +
                 "\t--log, -g               write logs to file (default: stdout)\n" +
                 "\t--help, -h              list of available options and usages\n";

    cout << msg;
}

void CommandParser::version() {
    string msg = "\nVersion 1.0\n\n";
    cout << msg;
}

void CommandParser::help() {
    cout << "RinsLR (Discovery of mid-range novel insertion sequences using long-reads)\n";
    version();
    string msg;
    msg = string("Usage:    rinslr <command> [options]\n\n") +
            "Command:    index        Build an index from the long-reads\n" +
            "            cluster      Identify potential breakpoint locations and create clusters from the short-reads\n" +
            "            find         Identify long-read candidates\n" +
            "            call         Detect novel sequence insertions\n";
    cout << msg;
}
