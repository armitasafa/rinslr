#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <tuple>
#include <cmath>
#include <ctime>
#include "aligner.h"
#include "common.h"

#include "parasail.h"
#include "parasail/matrix_lookup.h"
#include "parasail/matrices/dnafull.h"

using namespace std;

aligner::aligner(int reflen, int qlen)
{
	MAX_SIDE_A = reflen;
	MAX_SIDE_B = qlen;
	score = new int * [MAX_SIDE_A + 1];
	gapa  = new int * [MAX_SIDE_A + 1];
	gapb  = new int * [MAX_SIDE_A + 1];

	for (int i = 0; i <= MAX_SIDE_A; i++) {
		score[i] = new int[MAX_SIDE_B + 1];
		gapa[i] = new int[MAX_SIDE_B + 1];
		gapb[i] = new int[MAX_SIDE_B + 1];
	}

	score[0][0] = gapa[0][0] = gapb[0][0] = 0;
	for (int i = 1; i <= MAX_SIDE_A; i++) {
		score[i][0] = 0;
		gapa[i][0] = GAP_OPENING_SCORE + (i - 1) * GAP_EXTENSION_SCORE;
		gapb[i][0] = GAP_OPENING_SCORE + (i - 1) * GAP_EXTENSION_SCORE;
	}
	for (int i = 1; i <= MAX_SIDE_B; i++) {
		score[0][i] = GAP_OPENING_SCORE + (i-1) * GAP_EXTENSION_SCORE;
		gapa[0][i] = GAP_OPENING_SCORE + (i-1) * GAP_EXTENSION_SCORE;
		gapb[0][i] = GAP_OPENING_SCORE + (i-1) * GAP_EXTENSION_SCORE;
	}
	a.reserve(MAX_SIDE_B);
	b.reserve(MAX_SIDE_B);
	c.reserve(MAX_SIDE_B);
}
/**********************************************************/
aligner::~aligner() {
	for (int i = 0; i <= MAX_SIDE_A; i++) {
		delete[] score[i];
		delete[] gapa[i];
		delete[] gapb[i];
	}
	delete[] score;
	delete[] gapa;
	delete[] gapb;
}
/*********************************************************/
void aligner::print_matrix(string name, const string &a, const string &b, int **matrix) {
    Logger::instance().info("=================================\n");
    Logger::instance().info("Matrix: %s\n", name.c_str());
    Logger::instance().info("=================================\n");
    Logger::instance().info("       -");
	for (int i = 0; i < a.length(); i++)
        Logger::instance().info("%4c", a[i]);
    Logger::instance().info("\n");
	for (int j = 0; j <= b.length(); j++) {
		if (j > 0)
		    Logger::instance().info("%4c", b[j - 1]);
		else
		    Logger::instance().info("   -");
		for (int i = 0; i <= a.length(); i++) {
			if (matrix[i][j] <= -100000)
                Logger::instance().info("   N");
			else
                Logger::instance().info("%4d", matrix[i][j]);
		}
        Logger::instance().info("\n");
	}
    Logger::instance().info("=================================\n");
}
/**************************************************/
void aligner::clear(int a, int b) {
	identity = 0;

	for (int i = 0; i < a; i++)
		for (int j = 0; j < b; j++)
			score[i][j] = 0;

	for (int i = 0; i < b; i++) {
		gapa[0][i] = -100000;
		for (int j = 1; j < a; j++)
			gapa[j][i] = 0;
	}
	for (int j = 0; j < a; j++) {
		gapb[j][0] = -100000;
		for (int i = 1; i<b; i++)
			gapb[j][i] = 0;
	}
}
/**************************************************/
void aligner::empty() {
	identity = 0;
    score[0][0] = gapa[0][0] = gapb[0][0] = 0;
    for (int i = 1; i <= MAX_SIDE_A; i++) {
        score[i][0] = 0;
        gapa[0][i] = gapa[i][0] = GAP_OPENING_SCORE + (i - 1) * GAP_EXTENSION_SCORE;
        gapb[0][i] = gapb[i][0] = GAP_OPENING_SCORE + (i - 1) * GAP_EXTENSION_SCORE;
    }
    for (int i = 1; i <= MAX_SIDE_B; i++) {
        score[0][i] = GAP_OPENING_SCORE + (i-1) * GAP_EXTENSION_SCORE;
        gapa[0][i] = GAP_OPENING_SCORE + (i-1) * GAP_EXTENSION_SCORE;
        gapb[0][i] = GAP_OPENING_SCORE + (i-1) * GAP_EXTENSION_SCORE;
    }
}
/*************************************************/
void aligner::align(const string &ref, const string &ass) {
    clipped = false;
	
	parasail_result_t *result = NULL;
	const parasail_matrix_t *matrix = NULL;
	
	parasail_matrix_t *user_matrix = NULL;
	user_matrix = parasail_matrix_create("ACGT", 2, -1);

	result = parasail_sg_trace(ass.c_str(), ass.length(), ref.c_str(), ref.length(), 10, 1, user_matrix);
    // result = parasail_sg_trace(ass.c_str(), ass.length(), ref.c_str(), ref.length(), 10, 1, &parasail_dnafull);

   parasail_traceback_t* t = parasail_result_get_traceback(result, ass.c_str(), ass.length(), ref.c_str(), ref.length(), user_matrix, '|', '*', '*');
	// parasail_traceback_t* t = parasail_result_get_traceback(result, ass.c_str(), ass.length(), ref.c_str(), ref.length(), &parasail_dnafull, '|', '*', '*');

	p_start = -1, p_end = -1;

	int i = 0;

	while (p_start < 0) {
		if ((t->ref[i] != '-' && t->query[i] == '-')) {
			i++;
		}
		else {
			p_start = i;
		}
	}

	i = p_start;
    int sc = -1;
    while (sc < 0) {
        if (t->ref[i] == '-' and t->query[i] != '-') {
            i++;
        }
        else {
            sc = i - p_start;
        }
    }

    if (sc >= 20) {
        clipped = true;
    }

    i = 0;

	i = (int)strlen(t->ref) - 1;
	while (p_end < 0) {
		if ((t->ref[i] != '-' && t->query[i] == '-')) {
			i--;
		}
		else {
			p_end = i;
		}
	}

	a=""; b=""; c="";

	for (int i = p_start; i <= p_end; i++) {
		a += t->ref[i];
		b += t->query[i];
		c += t->comp[i];
	}

	left_anchor = 0;
	right_anchor = 0;
	int mm = 0;
	int totalErr = 0;
	for (int i = 0; i < a.length(); i++) {
		if (a[i] == b[i]) {
			left_anchor++;
			mm = 0;
		}
		else {
			mm++;
			totalErr++;
			left_anchor++;
		}
		if (mm == 2) {
			left_anchor -= 2;
			break;
		}
		if (i > 10 && totalErr > (float(i) / 5.0))
			break;
	}
	mm = 0;
	totalErr = 0;
	for (int i = a.length() - 1; i >= 0; i--) {
		if (a[i] == b[i]) {
			right_anchor++;
			mm = 0;
		}
		else {
			mm++;
			totalErr++;
			right_anchor++;
		}
		if (mm == 2) {
			right_anchor-=2;
			break;
		}
		if (i < a.length() - 10 && totalErr > (float(a.length() - float(i)) /5.0)) {
		    break;
		}
	}

	int errors = 0;
	for (int i = 0; i < a.length(); i++) {
		if (a[i] != b[i]) {
			errors += 1;
		}
	}

	int len = ass.length();
	int l = 1 + abs((((p_end - p_start) + 1) - len));
	identity = 1 - (errors + log(l)) / len;

	parasail_result_free(result);
}
/**********************************************************************/
int aligner::extract_calls( const int &cluster_id, vector<tuple<string, int, int, string, int, float > > &reports_lq,
                            vector<tuple<string, int, int, string, int, float > > &reports, const int &contig_support,
                            const int &ref_start, string direction) {
    ref_abs_start = ref_start + p_start;
    dump(direction);

    int mapped					= 0;
    int insertion_start_loc 	= -1;
    int insertion_end_loc		= -1;
    double fwdIden 				= get_identity();
    string insertion_content;
    int fwdS;

    if (fwdIden > 1.0 && (left_anchor > 50 || right_anchor > 50)) {
        fwdS = ref_start + p_start;
        int del_len				= 0;
        int ins_len				= 0;
        int deletion_start_loc 	= 0;
        int deletion_end_loc 	= 0;
        int isdeletion			= 0;
        string deletion_content;
        if (b.length() > 0) {
            int p = 0;
            while(p < b.length()) {
                del_len=0;
                for(; p < b.length(); p++) {
                    if (b[p] == '-') {
                        deletion_start_loc = p;
                        del_len = 1;
                        break;
                    }
                }
                for (; p < b.length(); p++) {
                    if (b[p] != '-') {
                        deletion_end_loc = p;
                        break;
                    }
                    else
                        del_len++;
                }
                if ((del_len >= 5) && (deletion_start_loc != -1) && ((deletion_start_loc + 1) != insertion_end_loc)) {
                    isdeletion         = 1;
                    deletion_content   = a.substr(deletion_start_loc,deletion_end_loc-deletion_start_loc);
                    deletion_start_loc = deletion_start_loc + fwdS;

                    if ((deletion_content.length()) > 0 && (fwdIden > 1.0) && (left_anchor > 5 && right_anchor > 5)
                    && (left_anchor > 16 || right_anchor > 16)) {
                        reports.push_back(tuple<string, int, int, string, int, float>("DEL", deletion_start_loc,
                                                                                      deletion_content.length(), deletion_content, contig_support, fwdIden ));
                    }
                }
            }
        }

        if (isdeletion == 0 && a.length() > 0) {
            int p 						= 0;
            int prevLen					= 0;

            while (p < a.length()) {
                ins_len = 0;
                for (; p < a.length(); p++) {
                    if (a[p] == '-') {
                        insertion_start_loc = p;
                        ins_len = 1;
                        break;
                    }
                }
                for (; p < a.length(); p++) {
                    if (a[p] != '-') {
                        insertion_end_loc = p;
                        break;
                    }
                    else
                        ins_len++;
                }
                if ((ins_len >= 5) && (insertion_start_loc != -1) && (insertion_start_loc + 1 != insertion_end_loc)) {
                    insertion_content   = b.substr(insertion_start_loc, insertion_end_loc - insertion_start_loc);
                    insertion_start_loc = insertion_start_loc + fwdS - 1;

                    insertion_start_loc = insertion_start_loc - prevLen;

                    if ((insertion_content.length() > 0) && (left_anchor > 5 && right_anchor > 5) &&
                    (left_anchor > 16 || right_anchor > 16)) {
                        Logger::instance().info("(-) %d\t%d\t%s\t%d (-)\n", insertion_start_loc, insertion_content.length(),
                                                insertion_content.c_str(), contig_support);
                        reports.push_back(tuple<string, int, int, string, int, float>("INS", insertion_start_loc,
                                          insertion_content.length(), insertion_content, contig_support, fwdIden));
                        mapped = 1;
                    }
                    else if (insertion_content.length() > 0 && (left_anchor >= 16 || right_anchor >= 16)) {
                        reports_lq.push_back(tuple<string, int, int, string, int, float>("INS", insertion_start_loc,
                                                                                         insertion_content.length(), insertion_content, contig_support, fwdIden ) );
                    }
                }
                prevLen += ins_len;
                p++;
            }
        }
    }
    return mapped;
}
/********************************************************************************/
int aligner::extract_calls( const int &cluster_id, vector<tuple<string, int, int, string, int, float > > &reports_lq,
                            vector<tuple<string, int, int, string, int, float > > &reports, const int &contig_support,
                            const int &ref_start, string direction, string &output) {
    ref_abs_start = ref_start + p_start;
	dump(direction, output);

	int mapped					= 0;
	int insertion_start_loc 	= -1;
	int insertion_end_loc		= -1;
	double fwdIden 				= get_identity();
	string insertion_content;
	int fwdS;

//	if (fwdIden > 1.0 && (left_anchor > 50 || right_anchor > 50)) {
	if (fwdIden > 1.0 && !clipped) {
		fwdS = ref_start + p_start;
		int del_len				= 0;
		int ins_len				= 0;
		int deletion_start_loc 	= 0;
		int deletion_end_loc 	= 0;
		int isdeletion			= 0;
		string deletion_content;
		if (b.length() > 0) {
			int p = 0;
			while (p < b.length()) {
				del_len = 0;
				for (; p < b.length(); p++) {
					if (b[p] == '-') {
						deletion_start_loc = p;
                        del_len = 1;
                        break;
					}
				}
				for(; p < b.length(); p++) {
					if (b[p] != '-') {
						deletion_end_loc = p;
						break;
					}
					else
						del_len++;
				}
				if ((del_len >= 5) && (deletion_start_loc != -1) && ((deletion_start_loc + 1) != insertion_end_loc)) {
					isdeletion         = 1;
					deletion_content   = a.substr(deletion_start_loc, deletion_end_loc - deletion_start_loc);
					deletion_start_loc = deletion_start_loc+fwdS;

					if ((deletion_content.length() > 0) && (fwdIden > 1.0) && (left_anchor > 5 && right_anchor > 5)
					    && (left_anchor > 16 || right_anchor > 16)) {
						reports.push_back(tuple<string, int, int, string, int, float>("DEL", deletion_start_loc,
                                          deletion_content.length(), deletion_content, contig_support, fwdIden));
					}
				}
			}
		}

		if (isdeletion == 0 and a.length() > 0) {
			int p 						= 0;
			int prevLen					= 0;

			while (p < a.length()) {
			    int curr_left_anchor = 0, curr_right_anchor = 0;
				ins_len = 0;
				for(; p < a.length(); p++) {
					if (a[p] == '-') {
						insertion_start_loc = p;
                        ins_len = 1;
                        break;
					}
					else {
					    curr_left_anchor += 1;
					}
				}
				for (; p < a.length(); p++) {
					if (a[p] != '-') {
						insertion_end_loc = p;
						int q = p;
						while (q < a.length() && a[q] != '-') {
						    curr_right_anchor += 1;
						    q++;
						}
						break;
					}
					else
						ins_len++;
				}

				if ((ins_len >= 5) && (insertion_start_loc != -1) && ((insertion_start_loc + 1) != insertion_end_loc)) {
					insertion_content   = b.substr(insertion_start_loc, insertion_end_loc - insertion_start_loc);
					insertion_start_loc = insertion_start_loc+fwdS - 1;
                    insertion_start_loc = insertion_start_loc-prevLen;

                if (insertion_content.length() > 0 && (curr_left_anchor > 5 && curr_right_anchor > 5) &&
                    (curr_left_anchor > 16 || curr_right_anchor > 16)) {
                        output += "(-) " + to_string(insertion_start_loc) + "\t" + to_string(insertion_content.length())
                                + "\t" + insertion_content + "\t" + to_string(contig_support) + " (-)\n";
                        //TODO: Remove hard-coded threshold
						if (insertion_content.length() > 19)
							reports.push_back(tuple<string, int, int, string, int, float>("INS", insertion_start_loc,
                                              insertion_content.length(), insertion_content, contig_support, fwdIden ) );
						else
							output += "Insertion too short. Not reported\n";
						mapped = 1;
					}

                    else if (insertion_content.length() > 0 && (curr_left_anchor >= 16 || curr_right_anchor >= 16)) {
						reports_lq.push_back(tuple<string, int, int, string, int, float>("INS", insertion_start_loc,
                                             insertion_content.length(), insertion_content, contig_support, fwdIden));
					}
				}
				prevLen += ins_len;
				p++;
			}
		}
	}
	return mapped;
}
/********************************************************************************/
int aligner::extract_calls( const int &cluster_id, vector<tuple<string, int, int, string, int, float > > &reports_lq,
                            vector<tuple<string, int, int, string, int, float, int, int, int > > &reports, const int &contig_support,
                            const int &ref_start, string direction, string &output) {
    ref_abs_start = ref_start + p_start;
    dump(direction, output);

    vector<event> events;

    int mapped					= 0;
    int insertion_start_loc 	= -1;
    int insertion_end_loc		= -1;
    double fwdIden 				= get_identity();
    string insertion_content;
    int fwdS;

    if (fwdIden > 1.0 && !clipped) {
        fwdS = ref_start + p_start;

        int lf = 0;
        int rf = 0;
        int f = 0;
        int ins_start, ins_end;
        int total_ins_lens = 0;
        int cnt = 0;
        int inslen = 0;
        event_en type;

        for (int i = 0; i < a.length(); i++) {
            if (a[i] != '-' && b[i] != '-') {
                if (c[i] == '|') {
                    if (f == 0) {
                        lf++;
                    }
                    else if (f == 1) {
                        rf++;
                        f = 2;
                    }
                    else if (f == 2) {
                        rf++;
                    }
                }
            }
            else if ((a[i] == '-' && b[i] != '-') || (a[i] != '-' && b[i] == '-')) {
                if (f == 0) {
                    ins_start = i;
                    f = 1;
                    if (a[i] == '-' && b[i] != '-') {
                        type = INS;
                    }
                    else if (a[i] != '-' && b[i] == '-') {
                        type = DEL;
                    }
                }
                else if (f == 2) {
                    //end of event
                    cnt++;
                    ins_end = ins_start + inslen;
                    event new_event;
                    new_event.left_flank = lf;
                    new_event.right_flank = rf;
                    new_event.len = inslen;
                    new_event.content = b.substr(ins_start, inslen);
                    new_event.start = ins_start + fwdS - 1 - total_ins_lens;
                    new_event.end = ins_end - total_ins_lens;
                    new_event.type = type;
                    events.push_back(new_event);

                    if (type == INS) {
                        total_ins_lens += inslen;
                    }

                    lf = rf;
                    rf = 0;
                    ins_start = i;
                    inslen = 0;
                    f = 1;

                    if (a[i] == '-' && b[i] != '-') {
                        type = INS;
                    }
                    else if (a[i] != '-' && b[i] == '-') {
                        type = DEL;
                    }
                }
                inslen++;
            }
        }

        if (f == 2) {
            cnt++;
            ins_end = ins_start + inslen;
            event new_event;
            new_event.left_flank = lf;
            new_event.right_flank = rf;
            new_event.len = inslen;
            new_event.content = b.substr(ins_start, inslen);
            new_event.start = ins_start + fwdS - 1 - total_ins_lens;
            new_event.end = ins_end - total_ins_lens;
            new_event.type = type;
            events.push_back(new_event);

            if (type == INS) {
                total_ins_lens += inslen;
            }
        }

        for (int i = 0; i < events.size(); i++) {
            events[i].cluster_total = cnt;

            int ins_len = events[i].len;
            int insertion_start_loc = events[i].start;
            int insertion_end_loc = events[i].end;
            string insertion_content = events[i].content;
            int curr_left_anchor = events[i].left_flank;
            int curr_right_anchor = events[i].right_flank;

            if (events[i].type == INS && curr_left_anchor > MIN_ANCHOR && curr_right_anchor > MIN_ANCHOR) {
                output += "(-) " + to_string(insertion_start_loc) + "\t" + to_string(insertion_content.length())
                          + "\t" + insertion_content + "\t" + to_string(contig_support) + " (-)\n";

                if (insertion_content.length() >= INS_MIN_LEN) {
                    reports.push_back(
                            tuple<string, int, int, string, int, float, int, int, int>("INS", insertion_start_loc,
                                                                                       insertion_content.length(),
                                                                                       insertion_content,
                                                                                       contig_support, fwdIden,
                                                                                       curr_left_anchor,
                                                                                       curr_right_anchor, cnt));
                } else {
                    output += "Insertion too short. Not reported\n";
                }
                mapped = 1;
            }
        }
    }
    return mapped;
}
/********************************************************************************/
void aligner::dump(string direction) {
	string cnt = "";

	for (int i = 1; i <= a.length(); i++) {
		if (i % 10==0)
			cnt += '0';
		else if (i % 5==0)
			cnt += '5';
		else
			cnt += ' ';
	}
    Logger::instance().info("\n%s I:%6.2f   %s\nG(%10d): %s\n               %s\nA(%10d): %s\n", direction.c_str(),
                            get_identity(), cnt.c_str(), ref_abs_start,a.c_str(), c.c_str(), 1,b.c_str());
}/********************************************************************************/
void aligner::dump(string direction, string& output) {
	string cnt = "";
	for (int i = 1; i <= a.length(); i++) {
		if (i % 10==0)
			cnt += '0';
		else if(i % 5==0)
			cnt += '5';
		else
			cnt += ' ';
	}

	char identity[20], gl[20], al[20];
	sprintf(identity, "%6.2f", get_identity());
    sprintf(gl, "%10d", ref_abs_start);
    sprintf(al, "%10d", 1);

    output += "\n" + direction + " I:" + identity + "   " + cnt + "\nG(" + gl + "): " + a + "\n               "
            + c + "\nA(" + al + "): " + b + "\n";
}
/********************************************************************************/
int aligner::get_start() {
	return p_start;
}
/********************************************************************************/
int aligner::get_end() {
	return p_end;
}
/********************************************************************************/
float aligner::get_identity() {
	return 100 * identity;
}
/********************************************************************************/
string aligner::get_a() {
	return a;
}
/********************************************************************************/
string aligner::get_b() {
	return b;
}
/********************************************************************************/
int aligner::get_left_anchor() {
	return left_anchor;
}
/********************************************************************************/
int aligner::get_right_anchor() {
	return right_anchor;
}