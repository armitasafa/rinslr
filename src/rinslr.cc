#include <utility>
#include "sketch.h"
#include "process_partition.h"
#include "partition_hybrid.h"
#include "command_parser.h"
#include "LRIdentification.h"

#include "parasail.h"
#include "parasail/matrices/blosum62.h"
#include "parasail/matrix_lookup.h"

#include <vector>

using namespace std;
unsigned char mute = 0;

/*********************************************************************************************/
void sketch (const string &longread, const string &dat_path, int t, int k, int w, float discard_freq)
{
    Sketch lr_sketch = Sketch(longread, dat_path, t, k, w, discard_freq);
}
/*********************************************************************************************/
void consensus (const string &partition_file, const string &dat_path, const string &reference, const string lr_path,
                const string &range, const string &prefix, const int t, const int max_len)
{
    ProcessPartition processor = ProcessPartition(t, lr_path, dat_path, partition_file, range, reference,
                                                  prefix, max_len);
    processor.process();
}
/*********************************************************************************************/
int main(int argc, char **argv)
{
    CommandParser parser;
    int err = parser.parse(argc, argv);

    if (err == 0) {
        //TODO: fix logger
        switch (parser.mode) {
            case CLUSTER: {
                genome_partition_hybrid pt(parser.out_dir_prefix, true, parser.td);
                pt.cluster_reads(parser.short_reads_file);
                break;
            }
            case INDEX: {
                sketch(parser.long_reads_file, parser.out_dir_prefix, parser.threads, parser.kmer_size, parser.window_size, parser.discard_freq);
                break;
            }
            case FIND: {
                Logger::instance().debug.set_file("debug.find.log");
                LRIdentification identification(parser.partition_path, parser.dat_path, parser.range, parser.reference_file,
                           parser.output_prefix, parser.long_reads_file, parser.threads, parser.tga, parser.ti,
                           parser.tck, parser.out_dir_prefix, parser.anchor_len);
                identification.identify();
                break;
            }
            case CALL: {
                Logger::instance().debug.set_file("debug.call.log");
                consensus(parser.partition_path, parser.dat_path, parser.reference_file, parser.long_reads_file,
                          parser.range, parser.output_prefix, parser.threads, parser.max_len);
                break;
            }
            default: {
                break;
            }
        }
    }
    return 0;
}
