#export
CXX ?= g++
RELEASE_OPT = -O2 -Wno-narrowing
DEBUG_OPT = -g -O0
PROFILE_OPT = -O2 -pg -g
DEV_OPT = -O2 -DDEBUG
EXT_PATH = ext
SRC_PATH = src
BUILD_PATH = rinslr-obj
BIN_PATH = rinslr

EXT_LOGGER_PATH = $(EXT_PATH)/util-logger
EXT_LOGGER_INCLUDE=${EXT_LOGGER_PATH}/include

EXT_PROGRESSBAR_PATH = $(EXT_PATH)/util-progressbar
EXT_PROGRESSBAR_INCLUDE = $(EXT_PROGRESSBAR_PATH)/include

EXT_SMHASHER_PATH = $(EXT_PATH)/smhasher/
EXT_SMHASHER_INCLUDE = $(EXT_PATH)/smhasher/src
SMHASHER_SOURCES = $(EXT_SMHASHER_PATH)/src/MurmurHash3.cpp
SMHASHER_OBJECTS = $(patsubst $(EXT_SMHASHER_PATH)/src/%.cpp, $(BUILD_PATH)/smhasher/%.o, $(SMHASHER_SOURCES))

EXT_CLASP_PATH = $(EXT_PATH)/clasp/
EXT_CLASP_INCLUDE = $(EXT_CLASP_PATH)
CLASP_SOURCES = $(wildcard $(EXT_CLASP_PATH)/*.c)
CLASP_OBJECTS = $(patsubst $(EXT_CLASP_PATH)/%.c, $(BUILD_PATH)/clasp/%.o, $(CLASP_SOURCES))

EXT_SPOA_PATH = $(EXT_PATH)/spoa
EXT_SPOA_INCLUDE = $(EXT_SPOA_PATH)/include/spoa
EXT_SPOA_LIB = $(BUILD_PATH)/spoa/lib/libspoa.a

EXT_PARASAIL_PATH = $(EXT_PATH)/parasail
EXT_PARASAIL_INCLUDE = $(EXT_PARASAIL_PATH)

SRC_EXT = cc

SOURCE_FILES =  rinslr.cc \
		aligner.cc \
		common.cc \
		genome.cc \
		partition.cc \
		assembler.cc \
		sam_processing.cc \
		sketch.cc \
		p2_partition.cc \
		p3_partition.cc \
		cut_ranges.cc \
		insertion_assembler.cc \
		partition_hybrid.cc \
		process_partition.cc \
		chain.cc \
		command_parser.cc \
		LRIdentification.cc

MAIN_SOURCES = $(patsubst %, $(SRC_PATH)/%, $(SOURCE_FILES))
MAIN_OBJECTS = $(MAIN_SOURCES:$(SRC_PATH)/%.$(SRC_EXT)=$(BUILD_PATH)/%.o)

OBJECTS = $(CLASP_OBJECTS) $(SMHASHER_OBJECTS) $(MAIN_OBJECTS)

.SECONDARY: $(UTIL_OBJ)

EXE=rinslr
DEPS = $(OBJECTS:.o=.d) $(UTIL_OBJ:.o=.d) $(PROCESSING_OBJ:.o=.d)
COMPILE_FLAGS = -std=c++14 #-Wall -Wextra #Removed because rinslr gives way too many warnings

INCLUDES = -I $(SRC_PATH)/include \
	-I $(EXT_LOGGER_INCLUDE) \
	-I $(EXT_PROGRESSBAR_INCLUDE) \
	-I $(EXT_SMHASHER_INCLUDE) \
	-I $(EXT_SPOA_INCLUDE) \
	-I $(EXT_CLASP_INCLUDE) \
	-I $(EXT_PARASAIL_INCLUDE)

LFLAGS = $(LDFLAGS) -lm -lz -L$(BUILD_PATH)/parasail -lparasail -L$(BUILD_PATH)/spoa/lib -lspoa -lpthread -lssl

.PHONY: default_make
default_make: release
.PHONY: release
.PHONY: debug
.PHONY: profile
.PHONY: install
.PHONY: dev

release: export CXXFLAGS := $(CXXFLAGS) $(COMPILE_FLAGS) $(RELEASE_OPT)
release: dirs
	@$(MAKE) all
debug: export CXXFLAGS := $(CXXFLAGS) $(COMPILE_FLAGS) $(DEBUG_OPT)
debug: dirs
	@$(MAKE) all
dev: export CXXFLAGS := $(CXXFLAGS) $(COMPILE_FLAGS) $(DEV_OPT)
dev: dirs
	@$(MAKE) all
profile: export CXXFLAGS := $(CXXFLAGS) $(COMPILE_FLAGS) $(PROFILE_OPT)
profile: dirs
	@$(MAKE) all
install:  export CXXFLAGS := $(CXXFLAGS) $(COMPILE_FLAGS) $(RELEASE_OPT)
install: dirs
	@$(MAKE) install_helper
.PHONY: dirs
dirs:
	@mkdir -p $(BUILD_PATH)
	@mkdir -p $(BUILD_PATH)/clasp
	@mkdir -p $(BUILD_PATH)/smhasher
	@mkdir -p $(BIN_PATH)

clean: build_clean bin_clean
.PHONY: clean
build_clean:
	@$(RM)  $(OBJECTS)
	@$(RM)  $(DEPS)
	@$(RM) -rf $(BUILD_PATH)
.PHONY: build_clean
bin_clean:
	$(RM)  $(BIN_PATH)/$(EXE)
	$(RM)  $(BIN_PATH)/version
	$(RM) -d $(BIN_PATH)
-include $(DEPS)

$(BUILD_PATH)/spoa: $(EXT_SPOA_PATH)
	@echo Preparing SPOA 
	@cmake -DCMAKE_CXX_COMPILER=$(CXX) -DCMAKE_C_COMPILER=gcc -Dspoa_build_executable=OFF -DCMAKE_BUILD_TYPE=Release -B$(BUILD_PATH)/spoa -H$(EXT_SPOA_PATH) -Wno-dev 1>/dev/null  2>/dev/null

$(EXT_SPOA_LIB): $(BUILD_PATH)/spoa
	@echo Building SPOA Library
	@make -C $(BUILD_PATH)/spoa > /dev/null

$(BUILD_PATH)/parasail: $(EXT_PARASAIL_PATH)
	@echo Preparing Parasail Library
	@mkdir $(BUILD_PATH)/parasail
	@cmake -DCMAKE_CXX_COMPILER=$(CXX) -DCMAKE_C_COMPILER=gcc -DBUILD_SHARED_LIBS=OFF -B$(BUILD_PATH)/parasail $(EXT_PARASAIL_PATH) -Wno-dev 1>/dev/null  2>/dev/null
	@make -C $(BUILD_PATH)/parasail > /dev/null

.PHONY: all
all: submodules $(BIN_PATH)/$(EXE)
	@git rev-parse --short HEAD > $(BIN_PATH)/version
.PHONY: install_helper
install_helper: $(BIN_PATH)/$(EXE) $(COPIED_SCRIPTS)
	@printf "\nPlease add rinslr to your path:\n\nexport PATH=\044PATH:`pwd`\n\n"

$(BIN_PATH)/$(EXE): $(EXT_SPOA_LIB) $(BUILD_PATH)/parasail $(OBJECTS)
	$(CXX) $(OBJECTS) $(LFLAGS) -o $@
$(BUILD_PATH)/smhasher/%.o: $(EXT_SMHASHER_PATH)/src/%.cpp
	$(CXX) $(CXXFLAGS) $(INCLUDES) -MP -MMD -c $< -o $@
$(BUILD_PATH)/clasp/%.o: $(EXT_CLASP_PATH)/%.c
	gcc -w -pedantic -std=c99 -O3 -DPROG3NFO -DDBGLEVEL=0 -c $< -o $@
$(BUILD_PATH)/%.o: $(SRC_PATH)/%.$(SRC_EXT)
	$(CXX) $(CXXFLAGS) $(INCLUDES) -MP -MMD -c $< -o $@

submodules:
	$(if $(wildcard $(EXT_LOGGER_PATH)/*), ,$(error Missing submodule util-logger, clone with --recursive)) 
	$(if $(wildcard $(EXT_PROGRESSBAR_PATH)/*), ,$(error Missing submodule util-progressbar, clone with --recursive)) 
	$(if $(wildcard $(EXT_SMHASHER_PATH)/*), ,$(error Missing submodule smhasher, clone with --recursive)) 
	$(if $(wildcard $(EXT_SPOA_PATH)/*), ,$(error Missing submodule spoa, clone with --recursive)) 
	$(if $(wildcard $(EXT_CLASP_PATH)/*), ,$(error Missing submodule clasp, clone with --recursive)) 
